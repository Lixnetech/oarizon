<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AssetMgmtController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() 
	{
		// INDEX PAGE
		$assets = Asset::all();
		$clients = Client::all();
		return View::make('assets.index', compact('assets','clients'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// NEW ASSET PAGE 
		$clients = Client::all(); 
		$assetNum = 'AST_000'.(Asset::all()->count()+1); $assetCats=Asset::all();//AssetCategory::all();
		return View::make('assets.create', compact('assetNum','clients','assetCats'));
	}   


	/**
	 * Store a newly created resource in storage.
	 * 
	 * @return Response
	 */
	public function store()
	{
		// STORE DATA IN DB
		$inputData=Input::all();
		$asset->quantity=Input::get('quantity');
		$asset_number=$inputData['assetNumber'];
		$asstcount=Asset::where('asset_number','=',$asset_number)->count();
if($asstcount>0){
			return Redirect::to('assetManagement1')->withDeleteMessage('Asset number already exists.Please try another one !');
	}else{
 	  $station = Input::get('station');
		if(!empty(Input::get('lifeYears'))){
			$inputData = array_merge(Input::all(), array('rate'=>'NULL', 'method'=>'years'));
		} elseif(!empty(Input::get('rate'))){
			$inputData = array_merge(Input::all(), array('lifeYears'=>'NULL', 'method'=>'rate'));
				}
		 $inputData;
		Asset::registerAsset($inputData);

		$assetAc = 'Accumulated Depreciation';
		$expAc = 'Depreciation Expense';
		$account1 = Account::where('name', $assetAc)->first();
		$account2 = Account::where('name', $expAc)->first();
		if(empty($account1)){
			Account::createAccount('ASSET', $assetAc, $inputData['purchasePrice']);
			if(empty($account2)){
				Account::createAccount('EXPENSE', $expAc);
			}
		} else{
			Account::where('name', $assetAc)->increment('balance', $inputData['purchasePrice']);
		}

		return Redirect::action('AssetMgmtController@index');
	}
   }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// DISPLAY ASSET INFORMATION
		$asset = Asset::find($id);
		$clients = Client::all();
		return View::make('assets.show', compact('asset','clients'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// DISPLAY EDIT PAGE
		$asset = Asset::find($id);
		$clients = Client::all();
		return View::make('assets.edit', compact('asset','clients'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// UPDATE DATA IN DB
		$asset->quantity=Input::get('quantity');
		$client->name = Input::get('name');
		if(!empty(Input::get('lifeYears'))){
			$inputData = array_merge(Input::all(), array('rate'=>'NULL', 'method'=>'years'));
		} elseif(!empty(Input::get('rate'))){
			$inputData = array_merge(Input::all(), array('lifeYears'=>'NULL', 'method'=>'rate'));
		}

		// Reverse Existing Depreciation
		/*$item = Asset::find($id);
		$creditAc = Account::where('name', 'Accumulated Depreciation')->pluck('id');
		$debitAc = Account::where('name', 'Depreciation Expense')->pluck('id');
		$lastDepAmnt = round($item->purchase_price - $item->book_value, 2);
		$item->increment('book_value', $lastDepAmnt);

		Account::where('id', $creditAc)->increment('balance', $lastDepAmnt);
		Account::where('id', $debitAc)->decrement('balance', $lastDepAmnt);*/

		// Update Details
		Asset::updateAsset($inputData);
		//$this->depreciate($id);

		return Redirect::action('AssetMgmtController@index');
	}


	/**
	 * Remove the specified resource from storage. 
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function destroy($id) 
	{
		Asset::find($id)->delete();
		return Redirect::route('assetManagement.index')->withDeleteMessage('successfully deleted!');
	} 
	public function dispose($id,$action)
	{
		$asset = Asset::findorFail($id);
		if($action=='dispose'){$status='Disposed'; $disposed=1;}else if($action=='undispose'){$status='Used'; $disposed=0;}
		$asset->status =$status;
		$asset->disposed =$disposed; 
		$asset->update();   
		/*if($action=='dispose'){
			//all journals to be affected
			$price=Input::get('price'); $depAmnt = Asset::calculateDepreciation($id);
			$creditAc = Account::where('name', $asset->asset_name)->pluck('id');
			$debitAc = Account::where('name', 'Disposal')->pluck('id');
			$data1 = [
				'date' => date("Y-m-d"),
				'debit_account' => $debitAc,
				'credit_account' =>$creditAc ,
				'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
				'amount' => $asset->purchase_price,
				'initiated_by' => Auth::user()->username,
				'particulars_id' => 0,
				'narration'=>Auth::user()->id
			];

			$acTransaction = new AccountTransaction;
			$journal = new Journal;
			$acTransaction->createTransaction($data1);
			$journal->journal_entry($data1);

			$creditAc2 = Account::where('name', 'Disposal')->pluck('id');		
			$debitAc2 = Account::where('name', 'Depreciation Expense')->pluck('id');
			$data2 = [
				'date' => date("Y-m-d"),
				'debit_account' => $debitAc2,
				'credit_account' =>$creditAc2 ,
				'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
				'amount' => $depAmnt,
				'initiated_by' => Auth::user()->username,
				'particulars_id' => 0,
				'narration'=>Auth::user()->id
			];

			$acTransaction2 = new AccountTransaction;
			$journal2 = new Journal;
			$acTransaction2->createTransaction($data2);
			$journal2->journal_entry($data2);

			$creditAc3 = Account::where('name', 'Disposal')->pluck('id');		
			$debitAc3 = Account::where('name', 'Cash Account')->pluck('id');
			$data3 = [
				'date' => date("Y-m-d"),
				'debit_account' => $debitAc3,
				'credit_account' =>$creditAc3,
				'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
				'amount' => $price,
				'initiated_by' => Auth::user()->username,
				'particulars_id' => 0,
				'narration'=>Auth::user()->id
			];

			$acTransaction3 = new AccountTransaction;
			$journal3 = new Journal;
			$acTransaction3->createTransaction($data3);
			$journal3->journal_entry($data3);

			$tprice=(int)$price+(int)$depAmnt;
			if((int)$tprice!=(int)$asset->purchase_price){
				$diff=(int)$asset->purchase_price-(int)$tprice;
				if($diff<0){
					$profit=$diff*1; $loss='none';
				}else{$loss=$diff; $profit='none';}
			
				if($loss='none'){
					$creditAc4 = Account::where('name', 'Disposal')->pluck('id');		
					$debitAc4 = Account::where('name', 'Profit/Loss')->pluck('id');
				}else{
					$creditAc4 = Account::where('name', 'Profit/Loss')->pluck('id');		
					$debitAc4 = Account::where('name', 'Disposal')->pluck('id');
				}
				$data4 = [
					'date' => date("Y-m-d"),
					'debit_account' => $debitAc4,
					'credit_account' =>$creditAc4,
					'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
					'amount' => $price,
					'initiated_by' => Auth::user()->username,
					'particulars_id' => 0,
					'narration'=>Auth::user()->id
				];

				$acTransaction4 = new AccountTransaction;
				$journal4 = new Journal;
				$acTransaction4->createTransaction($data4); 
				$journal4->journal_entry($data4);
			}
			//endAll journals to be affected 
		}*/

		$assets=Asset::all(); $members = Member::all();
		//return View::make('assets.index', compact('assets','members'));
		return Redirect::action('AssetMgmtController@index'); 
	}


	/**
	 * Depreciation Method
	 */
	public function depreciate($id){
		$item = Asset::find($id);

		$creditAc = Account::where('name', 'Accumulated Depreciation')->pluck('id');
		$debitAc = Account::where('name', 'Depreciation Expense')->pluck('id');
		$depAmnt = Asset::calculateDepreciation($id);

		$depDiff = $item->book_value - $depAmnt;

		$dta1 = [
			'date' => date("Y-m-d"),
			'debit_account' => $debitAc,
			'credit_account' => $creditAc,
			'description' => "Depreciated $item->asset_name on ". date('jS M, Y'),
			'initiated_by' => Auth::user()->username,
		];

		if($depDiff < $item->salvage_value){
			$dta = array('amount'=>$item->book_value - $item->salvage_value);
			$data = array_merge($dta1, $dta);
		} else{
			$data = array_merge($dta1, array('amount' => $depAmnt));
		}

		// Store depreciation values in DB
		if($item->book_value == $item->purchase_price){
			// Record Depreciation
			$item->decrement('book_value', $depAmnt);
			if($item->book_value == $item->salvage_value){
				return Redirect::action('AssetMgmtController@index');
			}

			$item->last_depreciated = date('Y-m-d');
			$item->update();

			Account::where('id', $creditAc)->decrement('balance', $depAmnt);
			Account::where('id', $debitAc)->increment('balance', $depAmnt);

			$acTransaction = new AccountTransaction;
			$journal = new Journal;

			$acTransaction->createTransaction($data);
			$journal->journal_entry($data);

			return Redirect::action('AssetMgmtController@index');
		} else{
			// Reverse already existing depreciation & Re-run
			$lastDepAmnt = round($item->purchase_price - $item->book_value, 2);

			$reverseData = [
				'date' => date("Y-m-d"),
				'debit_account' => $creditAc,
				'credit_account' => $debitAc,
				'description' => "Reversed depreciation on $item->asset_name on ". date('jS M, Y'),
				'amount' => $lastDepAmnt,
				'initiated_by' => Auth::user()->username
			];

			// Reverse Existing Depreciation
			$item->increment('book_value', $lastDepAmnt);

			Account::where('id', $creditAc)->increment('balance', $lastDepAmnt);
			Account::where('id', $debitAc)->decrement('balance', $lastDepAmnt);

			$acTransaction = new AccountTransaction;
			$journal = new Journal;

			$acTransaction->createTransaction($reverseData);
			$journal->journal_entry($reverseData);

			// Re-record Depreciation
			$item->decrement('book_value', $depAmnt);
			if($item->book_value == $item->salvage_value){
				return Redirect::action('AssetMgmtController@index');
			}

			$item->last_depreciated = date('Y-m-d');
			$item->update();

			Account::where('id', $creditAc)->decrement('balance', $depAmnt);
			Account::where('id', $debitAc)->increment('balance', $depAmnt);

			$acTransaction->createTransaction($data);
			$journal->journal_entry($data);

			return Redirect::action('AssetMgmtController@index');
		}
	}

}

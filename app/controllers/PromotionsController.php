<?php

class PromotionsController extends \BaseController {

	/**
	 * Display a listing of branches
	 *
	 * @return Response
	 */
	public function index()
	{
		$promotions = Promotion::whereNull('organization_id')->orWhere('organization_id',Confide::user()->organization_id)->get();

		Audit::logaudit('Promotions', 'view', 'viewed promotions');

		return View::make('promotions.index', compact('promotions'));
	}

	/**
	 * Show the form for creating a new branch
	 *
	 * @return Response
	 */
	public function create()
	{         $employees = Employee::all();
       $stations = Stations::all();
      $departments = Department::all();
       $jobtitles = JobTitle::whereNull('organization_id')->orWhere('organization_id',Confide::user()->organization_id)->get();


		//$employees = Employee::where('organization_id',Confide::user()->organization_id)->get();
		return View::make('promotions.create',compact('employees','stations','jobtitles','departments'));
	}

	/**
	 * Store a newly created branch in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
              $employeeid=Input::get('employee');
         $employee = Employee::findOrFail($employeeid);

		$validator = Validator::make($data = Input::all(), Promotion::$rules,Promotion::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		/**$promotion = new Promotion;

		$promotion->employee_id = Input::get('employee');

		$promotion->reason = Input::get('reason');

		$promotion->type = Input::get('type');

		$promotion->promotion_date = Input::get('date');

        $promotion->organization_id = Confide::user()->organization_id;

		$promotion->save();*/
         
       if((Input::get('operation'))=='promote')
     {
      $promo=new Promotion;

     $promo->employee()->associate($employee);
     $promo->salary=Input::get('salary');
      $promo->date=Input::get('pdate');
      $promo->department=Input::get('department');
       $promo->type='Promotion';
      $promo->reason=Input::get('reason');
      $promo->organization_id = Confide::user()->organization_id;

       $promo->save();
       return Redirect::route('promotions.index')->withFlashMessage('Promotion successfully created!');


        }
 if((Input::get('operation'))=='transfer')
     {
    $promo=new Promotion;

      $promo->employee()->associate($employee);
     $promo->salary=Input::get('salary');
      $promo->date=Input::get('tdate');
      $promo->stationto=Input::get('stationto');
      $promo->stationfrom=Input::get('stationfrom');
      $promo->reason=Input::get('reason');
      $promo->organization_id = Confide::user()->organization_id;

      $promo->type='Transfer';
      $promo->save();
  return Redirect::route('promotions.index')->withFlashMessage('Transfer successfully created!');



        }
      
    return Redirect::back();

       
        Audit::logaudit('Promotion', 'create', 'created: '.$promotion->type);

       /** if(Input::get('type') == 'Promotion'){
		return Redirect::route('promotions.index')->withFlashMessage('Promotion successfully created!');
      	}else if(Input::get('type') == 'Demotion'){
        return Redirect::route('promotions.index')->withFlashMessage('Demotion successfully created!');
      	}*/
	}

	/**
	 * Display the specified branch.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$promotion = Promotion::findOrFail($id);

		$employees = Employee::where('organization_id',Confide::user()->organization_id)->get();

		return View::make('promotions.show', compact('promotion','employees'));
	}

	/**
	 * Show the form for editing the specified branch.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$promotion = Promotion::find($id);

		$employees = Employee::where('organization_id',Confide::user()->organization_id)->get();

		return View::make('promotions.edit', compact('promotion','employees'));
	}

	/**
	 * Update the specified branch in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$promo = Promotion::findOrFail($id);
          
		$validator = Validator::make($data = Input::all(), Promotion::$rules,Promotion::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
                if((Input::get('operation'))=='promote')
     {
      

     //$promo->employee()->associate($employee);
     $promo->salary=Input::get('salary');
      $promo->date=Input::get('pdate');
      $promo->department=Input::get('department');
       $promo->type='Promotion';
      $promo->reason=Input::get('reason');
      $promo->organization_id = Confide::user()->organization_id;

       $promo->update();
       return Redirect::route('promotions.index')->withFlashMessage('Promotion successfully updated!');


        }
 if((Input::get('operation'))=='transfer')
     {
      //$promo->employee()->associate($employee);
     $promo->salary=Input::get('salary');
      $promo->date=Input::get('tdate');
      $promo->stationto=Input::get('stationto');
      $promo->stationfrom=Input::get('stationfrom');
      $promo->reason=Input::get('reason');
      $promo->organization_id = Confide::user()->organization_id;

      $promo->type='Transfer';
      $promo->update();
  return Redirect::route('promotions.index')->withFlashMessage('Transfer successfully updated!');



        }
      
    return Redirect::back();

       
        Audit::logaudit('Promotion', 'update', 'updated: '.$promotion->type);



		/**$promotion->employee_id = Input::get('employee');

		$promotion->reason = Input::get('reason');

		$promotion->type = Input::get('type');

		$promotion->promotion_date = Input::get('date');

		$promotion->update();

		 Audit::logaudit('Promotion', 'update', 'updated: '.$promotion->type);

		if(Input::get('type') == 'Promotion'){
		return Redirect::to('promotions')->withFlashMessage('Promotion successfully updated!');
      	}else if(Input::get('type') == 'Demotion'){
        return Redirect::to('promotions')->withFlashMessage('Demotion successfully updated!');
      	}*/
	}

	/**
	 * Remove the specified branch from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function promotionletter($id)
	{       
                 $promotion = Promotion::find($id);
                 $type=$promotion->type;
                 $employee = Employee::where('id','=',$promotion->employee_id)->get();
                 $department = Department::where('id','=',$promotion->department)->get();
                 $organization=Organization::find(1);

		//$promotions = Promotion::whereNull('organization_id')->orWhere('organization_id',Confide::user()->organization_id)->get();

		$pdf = PDF::loadView('pdf.promotionletter', compact('type','employee', 'organization','promotion'))->setPaper('a4')->setOrientation('potrait');

                 return $pdf->stream('Promotion Letter pdf');
	}
public function transferletter($id)
	{       
                 $promotion = Promotion::find($id);
                 $type=$promotion->type;
                 $employee = Employee::where('id','=',$promotion->employee_id)->first();
                 $stationto = Stations::where('id','=',$promotion->stationto)->first();
                 $stationfrom = Stations::where('id','=',$promotion->stationfrom)->first();
                  $organization=Organization::find(1);

                 //$promotions = Promotion::whereNull('organization_id')->orWhere('organization_id',Confide::user()->organization_id)->get();

		$pdf = PDF::loadView('pdf.transferletter', compact('type','stationto','stationfrom','employee', 'organization','promotion'))->setPaper('a4')->setOrientation('potrait');

                 return $pdf->stream('Transfer Letter pdf');
	}



	public function destroy($id)
	{
		$promotion = Promotion::findOrFail($id);
        
		Promotion::destroy($id);

        Audit::logaudit('promotion', 'delete', 'deleted: '.$promotion->type);

		if($promotion->type == 'Promote'){
		return Redirect::route('promotions.index')->withFlashMessage('Promotion successfully deleted!');
      	}else if($promotion->type == 'transfer'){
        return Redirect::route('promotions.index')->withFlashMessage('Transfer successfully deleted!');
      	}

}

}

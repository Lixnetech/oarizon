<?php

class ItemscategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /itemscategory
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /itemscategory/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('itemscategory.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /itemscategory
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		$category = new Itemscategory;


		$category->name = Input::get('name');
		$category->save();

		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 * GET /itemscategory/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = Itemscategory::find($id);
		$items = Item::where('category','=',$id)->get();
		return View::make('itemscategory.show',compact('category','items'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /itemscategory/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Itemscategory::find($id);

		return View::make('itemscategory.edit',compact('category'));

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /itemscategory/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//return $data = Input::all();

		$category = Itemscategory::find($id);

		$category->name = Input::get('name');
		$category->update();

		return Redirect::route('items.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /itemscategory/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$category = Itemscategory::destroy($id);

		return Redirect::route('items.index')->withDeleteMessage('Item successfully deleted');
	}

}
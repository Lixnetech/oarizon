<?php

class PettyCashController extends BaseController{

	/**
	 * Display a listing of expenses
	 *
	 * @return Response
	 */
	public function index(){
		Session::forget('newTransaction');
		Session::forget('trItems');

		$accounts = Account::all();
                $stations =Stations::all();
                $methods = Paymentmethod::all();
		$assets = Account::where('category', 'ASSET')->get();
		$expenses = Account::where('category', 'EXPENSE')->get();
		$liabilities = Account::where('category', 'LIABILITY')->get();
		$petty = Account::where('name', 'LIKE', '%petty%')->get();
		$petty_account = Account::where('name', 'LIKE', '%petty%')->where('active', 1)->first();
		$petty_expense = Account::where('name', 'LIKE', '%petty cash expense%')->where('active', 1)->first();
        $count=DB::table('pettycash_items')->distinct('ac_trns')->count('ac_trns');
                $voucher_number = str_pad($count+1, 3, "0", STR_PAD_LEFT);

		if(count($petty_account) > 0){
			$acID = $petty_account->id;

			$query = DB::table('account_transactions');
			$ac_transactions = $query->where(function($query) use ($acID){
										$query->where('account_debited', $acID)
										->orWhere('account_credited', $acID);
									})->orderBy('transaction_date','DESC')->get();
		return View::make('petty_cash.index', compact('accounts', 'assets','stations','voucher_number','methods','expenses','petty_expense', 'liabilities', 'petty', 'petty_account', 'ac_transactions'));
                 }
     else{
             return Redirect::back()->withInput()->withErrors('Create a petty Account first');
}
		//return $ac_transactions;

		
	}

	/**
	 * Show the form for creating a new expense
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created expense in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
	}

	/**
	 * Display the specified expense.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified expense.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	$transaction = PettycashItem::findOrFail($id);

		return View::make('petty_cash.edit', compact('transaction'));
	
	}

	/**
	 * Update the specified expense in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$pettyCashItem=PettycashItem::findOrFail($id);
                 $act_trans = AccountTransaction::where('id', $pettyCashItem->ac_trns)->first();
                 $init_amt=$pettyCashItem->quantity*$pettyCashItem->unit_price;
                  $diff=(Input::get('quantity')*Input::get('unit_price'));
                if($diff>$init_amt)
                 {
                 $amtt=$diff-$init_amt;
                 $new_amt=($act_trans->transaction_amount)+$amtt;
                  }
                else{
                 $amtt=$init_amt-$diff;
                 $new_amt=($act_trans->transaction_amount)-$amtt;
                  }


		
			$pettyCashItem->voucher_number = Input::get('voucher_number');
			$pettyCashItem->item_name = Input::get('item_name');
			$pettyCashItem->description =Input::get('description');
			$pettyCashItem->quantity = Input::get('quantity');
            $pettyCashItem->unit_price = Input::get('unit_price');

            
            $pettyCashItem->payee_name = Input::get('payee_name');
            $pettyCashItem->payee_no = Input::get('payee_no');
            
			$pettyCashItem->req_payment_date = Input::get('rqpay_date');
           
			

			$pettyCashItem->update();
                      $act_trans->transaction_amount=$new_amt;
                      $act_trans->update();

               
			return Redirect::back();
	}

	/**
	 * Remove the specified expense from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}


	/**
	 * SHOW RECEIPT TRANSACTIONS
	 */
	public function receiptTransactions($id){
                 $transid="";
                 $transid=$id;
                $firstitem=PettycashItem::where('ac_trns', $id)->first();
		$items = PettycashItem::where('ac_trns', $id)->get();

		return View::make('petty_cash.receiptTransactions', compact('items','firstitem','transid'));
	}

         /**
	 * Generate a voucher
	 */
            public function generateVoucher($id){
                 $transid="";
                 $organization = Organization::findOrFail(1);
                 $transid=$id;
                 $firstitem=PettycashItem::where('ac_trns', $id)->first();
                 $station=Stations::where('id','=', $firstitem->station_id)->first();
				$items = PettycashItem::where('ac_trns', $id)->get();
				$trans=AccountTransaction::find($transid); 

		//return View::make('petty_cash.voucher', compact('items','organization','firstitem','transid'));
                $pdf = PDF::loadView('petty_cash.voucher',compact('items','organization','station','firstitem','transid','trans'))->setOrientation('potrait')->setPaper('a4');
				return $pdf->stream('voucher pdf');
	}



public function submit_reimbursement(){
		$username=Confide::user()->username;
	    $amount=Input::get('amount');
		$users = DB::table('roles')
        ->join('assigned_roles', 'roles.id', '=', 'assigned_roles.role_id')
        ->join('users', 'assigned_roles.user_id', '=', 'users.id')
        ->join('permission_role', 'roles.id', '=', 'permission_role.role_id')
        ->where("permission_id",37)
        ->select("users.id","email","username")
        ->get();
        foreach ($users as $user) {
        
		 $message="Hello,You have a pending Request for Petty Cash reimbursement of KES. $amount from $username!";
		$type="petty cash reimbursement request"; $link="petty_cash"; $key=time()*time();
		Notification::notifyUser( $user->id, $message, $type, $link, $key);
         }
		return Redirect::action('PettyCashController@index')->with('psuccess', "Request for reimbursement successfully submitted");

	}


      
       public function approvereceipt($id){
		$items = PettycashItem::where('ac_trns', $id)->get();
		           $first=Confide::user()->first_name;
		           $last=Confide::user()->last_name;
		           $reason=Input::get('comment');
                 foreach($items as $item){
                 $item->is_approved=1;
                 $item->reject_comment=$reason;
                 $item->is_rejected=0;
                 $item->approval_date=date('Y-m-d');
                  $item->approved_by=$first."".$last;


                  $item->update();
                 }

		return Redirect::action('PettyCashController@index')->with('success', "Petty Cash Receipt Approved");

	}


	public function rejectreceipt($id){
		$items=PettycashItem::where('ac_trns', $id)->get();
		$firstitem=PettycashItem::where('ac_trns', $id)->first();

		
        $reason=Input::get('reject_comment');
    $user = DB::table('users')
        ->where("id",$firstitem->submit_user_id )->first();
		
                 foreach($items as $item){
                 $item->is_rejected=1;
                 $item->reject_comment=$reason;
                  


                  $item->update();
                 }
         $message="The request for approval has been rejected!.$reason";
		$type="Petty Cash Rejection"; $link="petty_cash"; $key=time()*time();
		Notification::notifyUser( $user->id, $message, $type, $link, $key);
		return Redirect::action('PettyCashController@index')->with('warning', "Petty Cash Receipt has been rejected");

	}



	/**
	 * ADD MONEY TO PETTY CASH ACCOUNT
	 */
	public function addMoney(){
		$ac_name = Account::where('id', Input::get('ac_from'))->first();
		$amount = Input::get('amount'); 
		$cash_in_hand=Input::get('cash_in_hand');
		if($ac_name->balance < Input::get('amount')){
			return Redirect::back()->with('error', 'Insufficient funds in From Account selected!');
		}
		$petty_acc = Account::where('name', 'LIKE', '%petty%')->where('active', 1)->first();
		if(count($petty_acc)>0){
			if($petty_acc->balance>=$cash_in_hand)
                         {$petty_undercash=$petty_acc->balance-$cash_in_hand;}else{$petty_undercash=0;}
			DB::table('accounts')->where('name','Petty Under-Cash Account')->increment('balance', $petty_undercash);
            DB::table('accounts')->where('name','Cash Account')->decrement('balance', $petty_undercash);

		}
		  $credit= Account::where('name', 'LIKE', '%Cash Account%')->where('category', 'ASSET')->where('active', 1)->first();
		  $debit= Account::where('name', 'LIKE', '%Under-Cash Account%')->where('category', 'EXPENSE')->where('active', 1)->first();
		  if($petty_undercash>0){
		$data1 = array(  
			'date' => date("Y-m-d"), 
			'debit_account' => $debit->id,
			'credit_account' => $credit->id,
			'description' => "Petty Under Cash Expense",
			'amount' => $petty_undercash,
			'initiated_by' => Confide::user()->username
		); 


		$journal = new Journal;

		$journal->journal_entry($data1);
         }

		$data = array(  
			'date' => date("Y-m-d"), 
			'debit_account' => Input::get('ac_to'),
			'credit_account' => Input::get('ac_from'),
			'description' => "Transferred cash from $ac_name->name account to Petty Cash Account",
			'amount' => Input::get('amount'),
			'initiated_by' => Confide::user()->username
		); 

		DB::table('accounts')->where('id', Input::get('ac_from'))->decrement('balance', Input::get('amount'));
		DB::table('accounts')->where('id', Input::get('ac_to'))->increment('balance', Input::get('amount'));

		$acTransaction = new AccountTransaction;
		$journal = new Journal;

		$acTransaction->createTransaction($data);
		$journal->journal_entry($data); 
		$userid=Confide::user()->id; $message="KES. $amount Successfully Transferred from $ac_name->name to Petty Cash!";
		$type="petty cash reimbursement"; $link="petty_cash"; $key=time()*time();
		Notification::notifyUser( $userid, $message, $type, $link, $key);

		return Redirect::action('PettyCashController@index')->with('success', "KES. $amount Successfully Transferred from $ac_name->name to Petty Cash!");
	}

	/**
	 * ADD MONEY TO PETTY CASH FROM OWNER'S CONTRIBUTION
	 */
	public function addContribution(){
		$ac_name = Account::where('id', Input::get('cont_acFrom'))->first();
		$contAmount = Input::get('cont_amount');
		$contName = Input::get('cont_name');

		$data = array(
			'date' => date("Y-m-d"), 
			'debit_account' => Input::get('cont_acTo'),
			'credit_account' => Input::get('cont_acFrom'),
			'description' => "Transferred Money to Petty Cash Account from $contName",
			'amount' => Input::get('cont_amount'),
			'initiated_by' => Confide::user()->username
		);

		DB::table('accounts')->where('id', Input::get('cont_acFrom'))->decrement('balance', Input::get('cont_amount'));
		DB::table('accounts')->where('id', Input::get('cont_acTo'))->increment('balance', Input::get('cont_amount'));

		$acTransaction = new AccountTransaction;
		$journal = new Journal;

		$acTransaction->createTransaction($data);
		$journal->journal_entry($data);

		return Redirect::action('PettyCashController@index')->with('success', "KES. $contAmount Transferred to Petty Cash Account from $contName");
	}

	/**
	 * CREATE NEW PETTY CASH TRANSACTION
	 */
	public function newTransaction(){
		Session::put('newTransaction', [
			'vouchernumber'=>Input::get('voucher_number'),
			'transactTo'=>Input::get('transact_to'),
			'trDate'=>Input::get('tr_date'),
			'description'=>Input::get('description'),
              'payeename'=>Input::get('payee_name'),
               'payeeno'=>Input::get('payee_no'),
              'paymethod'=>Input::get('pay_method'),
               'station'=>Input::get('station_id'),
              'requested_date'=>Input::get('rqpay_date'),

               'chequeno'=>Input::get('cheque_no'),
               'mpesacode'=>Input::get('mpesa_code'),
               'expense_ac'=>Input::get('expense_ac'),
			'credit_ac'=>Input::get('credit_ac')
		]);

		$newTr = Session::get('newTransaction');
		//return Input::get();
		if(Input::get('item') != NULL){
			Session::push('trItems', array(
                               
				'item_name' => Input::get('item'),
				'description' => Input::get('desc'),
				'quantity' => Input::get('qty'),
				'unit_price' => Input::get('unit_price')
			));
		}

		$trItems = Session::get('trItems');

		return View::make('petty_cash.transactionItems', compact('newTr', 'trItems'));
	}


	/**
	 * Remove Petty Cash Transaction Item
	 */
	public function removeTransactionItem($count){
		/*Session::put('newTransaction', [
			'transactTo'=>Input::get('transact_to'),
			'trDate'=>Input::get('tr_date')
		]);*/
		$newTr = Session::get('newTransaction');

		$items = Session::get('trItems');
		unset($items[$count]);
		$newItems = array_values($items);
		Session::put('trItems', $newItems);

		//return Session::get('trItems');
		$trItems = Session::get('trItems');

		return View::make('petty_cash.transactionItems', compact('newTr', 'trItems'));
	}
            
       
        
	/**
	 * Commit Transaction
	 */
	public function commitTransaction(){

		$newTr = Session::get('newTransaction');
		$trItems = Session::get('trItems');
		$petty_act=DB::table('accounts')->where('id', $newTr['credit_ac'])->first();

		if($trItems == NULL){
			return View::make('petty_cash.transactionItems', compact('newTr', 'trItems'));
		}
        
        
		$total = 0;
		foreach($trItems as $trItem){
			$total += ($trItem['quantity'] * $trItem['unit_price']);
		}
         if($petty_act->balance<$total){
        	return Redirect::action('PettyCashController@index')->withErrors("Failed.Insufficient funds in the petty cash account to perform the transaction. Request for Reimbursement");}
        	else{
				$chequeno=$newTr['chequeno']; $mpesa_code=$newTr['mpesacode']; $pay_method=$newTr['paymethod'];
				if($newTr['paymethod']=="Cheque"){$form_dets=$chequeno;}else if($newTr['paymethod']=="Mobile money"){$form_dets=$mpesa_code;}else{$form_dets="";}
		$data = array(
			'date' => date("Y-m-d"),
			'debit_account' => $newTr['transactTo'], 
			'credit_account' => $newTr['credit_ac'], 
			'description' => $newTr['description'],
			'pay_method' => $newTr['paymethod'],
			'pay_details' => $form_dets,  
			'amount' => $total,
			'initiated_by' => Confide::user()->username
		);

		DB::table('accounts')->where('id', $newTr['credit_ac'])->decrement('balance', $total);
		DB::table('accounts')->where('id', $newTr['transactTo'])->increment('balance', $total);

		$acTransaction = new AccountTransaction;
		$journal = new Journal;

		$trId = $acTransaction->createTransaction($data);
		$journal->journal_entry($data);
		foreach($trItems as $trItem){
			$pettyCashItem = new PettycashItem;

			$pettyCashItem->ac_trns = $trId;
			$pettyCashItem->voucher_number = $newTr['vouchernumber'];
			$pettyCashItem->item_name = $trItem['item_name'];
			$pettyCashItem->description = $trItem['description'];
			$pettyCashItem->quantity = $trItem['quantity'];
            $pettyCashItem->unit_price = $trItem['unit_price'];

            $pettyCashItem->transaction_date = $newTr['trDate'];
            $pettyCashItem->payee_name = $newTr['payeename'];
            $pettyCashItem->payee_no = $newTr['payeeno'];
            $pettyCashItem->station_id = $newTr['station'];
			$pettyCashItem->req_payment_date = $newTr['requested_date'];
            $pettyCashItem->mpesa_code = $newTr['mpesacode'];
			$pettyCashItem->cheque_no = $newTr['chequeno'];
			$pettyCashItem->submit_user_id = Confide::user()->id;

			$pettyCashItem->save();
		}

		Session::forget('newTransaction');
		Session::forget('trItems');

		return Redirect::action('PettyCashController@index');
	}
  }

}
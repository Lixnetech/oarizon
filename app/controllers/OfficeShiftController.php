<?php


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;


class OfficeShiftController extends \BaseController
{
    public function index()
    {

        $shifts = OfficeShift::all();

        return View::make('timesheet.work_shift.index',compact('shifts'));
    }

    public function create()
    {
        return View::make('timesheet.work_shift.create');
    }

    public  function store(){
        $validator = Validator::make(Input::all(),[
            'shift_name' => 'required'
        ]);

//        if($validator->fails()){
//            return Redirect::back()->with(['errors'=> $validator->errors()->all()]);
//        }

        $data = [];

        $data['shift_name'] = Input::get('shift_name');
        $data['monday_in'] = Input::get("monday_in");
        $data['monday_out'] =  Input::get("monday_out");
        $data['tuesday_in'] =  Input::get("tuesday_in");
        $data['tuesday_out'] =  Input::get("tuesday_out");
        $data['wednesday_in'] =  Input::get("wednesday_in");
        $data['wednesday_out'] =  Input::get("wednesday_out");
        $data['thursday_in'] =  Input::get("thursday_in");
        $data['thursday_out'] =  Input::get("thursday_out");
        $data['friday_in'] =  Input::get("friday_in");
        $data['friday_out'] =  Input::get("friday_out");
        $data['saturday_in'] = Input::get("saturday_in");
        $data['saturday_out'] = Input::get("saturday_out");
        $data['sunday_in'] =  Input::get("sunday_in");
        $data['sunday_out'] =  Input::get("sunday_out");
        $data['organization_id'] = 1;

        OfficeShift::create($data);

        return Redirect::to('timesheet/work_shift')->with('success','Data created Successfully');
    }

    public function edit($id)
    {
        $office_shift = OfficeShift::findOrFail($id);
        //$organization_name
        $orgs = Organization::select('id','organization')->get();

        return View::make('timesheet.office_shift.edit',compact(''));
    }

    public function destroy($id){
        OfficeShift::where('id',$id)->delete();
        return Redirect::to('timesheet/work_shift')->with('success','Data created Successfully');
    }
}
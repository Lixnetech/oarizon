<?php

class ParticularController extends \BaseController {

	 /**
		* Display a listing of the resource.
		*
		* @return Response
		*/
	 public function index()
	 {
			 $particulars = Particular::where('void', 0)->get()->toArray();
			 for ($i = 0; $i < count($particulars); $i++) {
				 if ($particulars[$i]['debitaccount_id'] != null){
					 $particulars[$i]['debit_account'] = Account::findOrFail($particulars[$i]['debitaccount_id'])->name;
				 }else{
					 $particulars[$i]['debit_account']= '';
				 }
				 if ($particulars[$i]['creditaccount_id'] != null){
					 $particulars[$i]['credit_account'] = Account::findOrFail($particulars[$i]['creditaccount_id'])->name;
				 }else{
					 $particulars[$i]['credit_account'] = '';
				 }
			 }
			 return View::make('Particular.index', compact('particulars'));
	 }


	 /**
		* Show the form for creating a new resource.
		*
		* @return Response
		*/
	 public function create()
	 {
			 $accounts = Account::all();
			 return View::make('Particular.create', compact('accounts'));
	 }


	 /**
		* Store a newly created resource in storage.
		*
		* @return Response
		*/
	 public function store()
	 {
			 $validator = Validator::make($data = Input::all(), array(
					 'name' => 'required',
					 'debit_account' => 'required',
					 'credit_account' => 'required'
			 ));

			 if ($validator->fails()) {
					 return Redirect::back()->withErrors($validator)->withInput();
			 }

			 $particular = new Particular();
			 $particular->name = Input::get('name');
			 $particular->debitaccount_id = Input::get('debit_account');
			 $particular->creditaccount_id = Input::get('credit_account');
			 if(Input::get('is_erp') != null){
			 $particular->is_erp = Input::get('is_erp');
			 }
			 $particular->save();
			 return Redirect::to('particulars');

	 }


	 /**
		* Display the specified resource.
		*
		* @param  int $id
		* @return Response
		*/
	 public function show($id)
	 {
			 //
	 }


	 /**
		* Show the form for editing the specified resource.
		*
		* @param  int $id
		* @return Response
		*/
	 public function edit($id)
	 {
			 $accounts = Account::all();

			 $particular = Particular::find($id);

			 return View::make('Particular.edit', compact('accounts', 'particular'));
	 }


	 /**
		* Update the specified resource in storage.
		*
		* @param  int $id
		* @return Response
		*/
	 public function update($id)
	 {
			 $validator = Validator::make($data = Input::all(), array(
					 'name' => 'required',
					 'debit_account' => 'required',
					 'credit_account' => 'required'
			 ));

			 if ($validator->fails()) {
					 return Redirect::back()->withErrors($validator)->withInput();
			 }

			 $particular = Particular::findOrFail($id);
			 $particular->name = Input::get('name');
			 $particular->debitaccount_id = Input::get('debit_account');
			 $particular->creditaccount_id = Input::get('credit_account');
			 if(Input::get('is_erp') != null){
			 $particular->is_erp = Input::get('is_erp');
			 }
			 $particular->save();

			 return Redirect::to('particulars');
	 }


	 /**
		* Remove the specified resource from storage.
		*
		* @param  int $id
		* @return Response
		*/
	 public function destroy($id)
	 {
			 $particular = Particular::find($id);
			 $particular->void = 1;
			 $particular->update();

			 return Redirect::to('particulars');
	 }


}

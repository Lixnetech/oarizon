<?php

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AssetCategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{  
		// INDEX PAGE 
		$assetCategories = AssetCategory::all();
		$clients = Client::all();   
		return View::make('assets.assetCategory.index', compact('assetCategories','clients'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{ 
		// NEW ASSET PAGE
		$clients = Client::all();
		$assetCategories = AssetCategory::all();
		return View::make('assets.assetCategory.create', compact('assetCategories','clients'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// STORE DATA IN DB
		$catName=Input::get('name');
		$assCats=AssetCategory::where('name',$catName)->get(); 
		if(count($assCats)>0){
			return Redirect::back()->withErrors('Category already exists');
		} 
		$assCat=new AssetCategory;
		$assCat->name=Input::get('name');
		$assCat->type = Input::get('type');  
		$assCat->save(); 

		return Redirect::action('AssetCategoryController@index')->withFlashMessage('new category suuccessfully added!');
	} 


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// DISPLAY ASSET INFORMATION
		$asset = Asset::find($id);
		$clients = Client::all();
		return View::make('assets.show', compact('asset','clients'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// DISPLAY EDIT PAGE
		$main_assetCat = AssetCategory::find($id); $assetCats=AssetCategory::where("id","!=",$id)->get();
		$clients = Client::all();
		return View::make('assets.assetCategory.edit', compact('main_assetCat','assetCats','clients'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// UPDATE DATA IN DB
		$assetCat=AssetCategory::find($id);
		$assetCat->name=Input::get('name');
		$assetCat->type = Input::get('type');
		$assetCat->update();  
		return Redirect::action('AssetCategoryController@index')->withFlashMessage('successfully updated!');
	}


	/** 
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{ 
		AssetCategory::destroy($id);
		return Redirect::back()->withFlashMessage('successfully deleted!');
	} 
 

	/**
	 * Depreciation Method
	 */

}

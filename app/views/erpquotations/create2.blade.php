@extends('layouts.erp')
@section('content')

<div class="row">
	<div class="col-lg-12">
  <h4><font color='green'>New Invoice</font></h4>

<hr>
</div>
</div>


<div class="row">
	<div class="col-lg-5">



		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif

		 <form method="POST" action="{{{ URL::to('erpquotations/create2') }}}" accept-charset="UTF-8">

    <fieldset>
        <font color="red"><i>All fields marked with * are mandatory</i></font>

         <div class="form-group">
            <label for="username">Invoice Reference Number:</label>
            <input type="text" name="order_number" value="{{$order_number}}" class="form-control">
        </div>

        <div class="form-group">
            <label for="username">Date</label>
            <div class="right-inner-addon ">
                <i class="glyphicon glyphicon-calendar"></i>
                <input class="form-control datepicker"  readonly="readonly" placeholder="" type="text" name="date" id="date" value="{{date('d-M-Y')}}">
            </div>
        </div>
       <div class="form-group">
                    <label for="username">LPO Number(Optional):</label>
                    <input type="text" name="lpo_no" value="" placeholder="supplier LPO no." class="form-control" >
                </div>

          <div class="form-group">
            <label for="username">Client <span style="color:red">*</span> :</label>
            <select name="client" class="form-control" required>
                @foreach($clients as $client)
                @if($client->type == 'Customer')
                    <option value="{{$client->id}}">{{$client->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="username">Bank Account<span style="color:red">*</span> :</label>
            <select name="bank" class="form-control" required>
                @foreach($bank_accounts as $bank_account)
                    <option value="{{$bank_account->id}}">{{$bank_account->bank_name}}</option>
                @endforeach
            </select>
        </div>

				<div class="form-group">

                        <input type="radio" class="form-check-input" name="type" value="all" checked>
						<label class="username" value="type">All</label>

						<input type="radio" class="form-check-input" name="type" value="service">
						<label class="username"  value="service">Service</label>

						 <input type="radio" class="form-check-input" name="type" value="product">
						 <label class="username" value="type">Product</label>
					</div>


       <!--  <div class="form-group">
            <label for="username">Purchase Type <span style="color:red">*</span> :</label>
            <select name="payment_type" class="form-control">

                    <option value="cash">Cash</option>
                    <option value="credit">Credit</option>

            </select>
        </div>
 -->


        <div class="form-actions form-group">

          <button type="submit" class="btn btn-primary btn-sm">Create</button>
        </div>

    </fieldset>
</form>


  </div>

</div>

@stop

@extends('layouts.erp')
@section('content')

<div class="row">
  <div class="col-lg-12">
  <h4><font color='green'>Quotations/Invoices</font></h4>

<hr>
</div>
</div>


<div class="row">
  <div class="col-lg-12">

    <!-- <div class="panel-heading">
        <a class="btn btn-info btn-sm pull-right" href="{{ URL::to('quotationorders/create3')}}">Product Only </a>
      </div> -->
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#quotations" aria-controls="remittance" role="tab"
                   data-toggle="tab">Quotations</a>
            </li>
            <li role="presentation">
                <a href="#invoices" aria-controls="profile" role="tab" data-toggle="tab">
                    Invoices</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="quotations">
                  <div class="panel panel-default" style="margin-bottom: 10px;">
                    <div class="panel-heading">Quotations
                      @if (Session::has('flash_message'))

                        <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                       </div>
                      @endif
                      @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

                      @if (Session::has('delete_message'))

                        <div class="alert alert-danger">
                        {{ Session::get('delete_message') }}
                       </div>
                      @endif

    <div class="panel panel-default">
      <div class="panel-heading">
          <a class="btn btn-info btn-sm" href="{{ URL::to('quotationorders/create')}}">New Quotation </a>
        </div>
        <div class="panel-body">


    <table id="users" class="table table-condensed table-bordered table-responsive table-hover">


      <thead>

        <th>#</th>
        <th>Client</th>
        <th>Quote #</th>
        <th>Date</th>
        <th>status</th>
        <th></th>

      </thead>
      <tbody>

        <?php $i = 1; ?>
        @foreach($quotations as $order)
        @if($order->type == 'quotations')
         @if($order->status != 'REJECTED')

        <tr>

          <td> {{ $i }}</td>
          <td>{{ $order->client->name }}</td>
          <td>{{$order->order_number }}</td>
          <td>{{$order->date }}</td>
          <td>{{$order->status }}</td>


          <td>

                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{URL::to('erpquotations/show/'.$order->id)}}">View</a></li>
                        <!-- <li><a href="{{URL::to('erpquotations/cancel/'.$order->id)}}"  onclick="return (confirm('Are you sure you want to cancel this quotation?'))">Cancel</a></li> -->

                    </ul>
                </div>

                    </td>



        </tr>

        <?php $i++; ?>
        @endif
        @endif
        @endforeach


      </tbody>


    </table>
  </div>

</div>
  </div>
</div>
</div>

<!--invoices -->
<div role="tabpanel" class="tab-pane" id="invoices">
      <div class="panel panel-default" style="margin-bottom: 10px;">
        <div class="panel-heading">Invoices
          @if (Session::has('flash_message'))

            <div class="alert alert-success">
            {{ Session::get('flash_message') }}
           </div>
          @endif
        
         @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

          @if (Session::has('delete_message'))

            <div class="alert alert-danger">
            {{ Session::get('delete_message') }}
           </div>
          @endif

          <div class="panel panel-default">
            <div class="panel-heading">
                  <a class="btn btn-info btn-sm" href="{{ URL::to('quotationorders/create2')}}">New Invoice</a>
              </div>
              <div class="panel-body">


          <table id="user2" class="table table-condensed table-bordered table-responsive table-hover">
            <thead>

              <th>#</th>
              <th>Client</th>
              <th>Quote #</th>
              <th>Date</th>
              <th>status</th>
              <th></th>

            </thead>
            <tbody>


                      <?php $i = 1; ?>
                      @foreach($quotations as $order)
                      @if($order->type == 'invoice')
                       @if($order->status != 'REJECTED')

                      <tr>

                        <td> {{ $i }}</td>
                        <td>{{ $order->client->name }}</td>
                        <td>{{$order->order_number }}</td>
                        <td>{{$order->date }}</td>
                        <td>{{$order->status }}</td>
                        <td>

                              <div class="btn-group">
                                  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                      Action <span class="caret"></span>
                                  </button>

                                  <ul class="dropdown-menu" role="menu">
                                      <li><a href="{{URL::to('erpquotations/show2/'.$order->id)}}">View</a></li>
                                      <!-- <li><a href="{{URL::to('erpquotations/cancel/'.$order->id)}}"  onclick="return (confirm('Are you sure you want to cancel this quotation?'))">Cancel</a></li> -->

                                  </ul>
                              </div>

                                  </td>

                          @endif
                         @endif
              </tr>

              <?php $i++; ?>
              @endforeach


            </tbody>





          </table>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>



@stop

@extends('layouts.erp')
@section('content')
<style>
    input[type=radio]{cursor:pointer; margin:0px 2px 0px 4px;}
</style>
<div class="row">
	<div class="col-lg-12">
  <h4><font color='green'>New Quotation</font></h4>

<hr>
</div>
</div>


<div class="row">
	<div class="col-lg-5">



		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif
	<?php 
		$count = DB::table('erporders')->count();
  		$order_number = date("Y/m/d/").str_pad($count+1, 4, "0", STR_PAD_LEFT);
		$items = Item::all();
  		$service = Item::where('type','=','service')->get();
  		$locations = Location::all();
  		$clients = Client::all();
	?>

		 <form method="POST" action="{{{ URL::to('erpquotations/create') }}}" accept-charset="UTF-8">

    <fieldset>
        <font color="red"><i>All fields marked with * are mandatorY</i></font>

         <div class="form-group">
            <label for="username">Quote Number:</label>
            <input type="text" name="order_number" value="{{$order_number}}" class="form-control" readonly>
        </div>

        <div class="form-group">
            <label for="username">Date</label>
            <div class="right-inner-addon ">
                <i class="glyphicon glyphicon-calendar"></i>
                <input class="form-control datepicker"  readonly="readonly" placeholder="" type="text" name="date" id="date" value="{{date('d-M-Y')}}">
            </div>
        </div>
         <div class="form-group">
                    <label for="username">LPO Number(Optional):</label>
                    <input type="text" name="lpo_no" value="" placeholder="supplier LPO no." class="form-control" >
                </div>


          <div class="form-group">
            <label for="username">Client <span style="color:red">*</span> :</label>
            <select name="client" class="form-control" required>
                @foreach($clients as $client)
                @if($client->type == 'Customer')
                    <option value="{{$client->id}}">{{$client->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="username">Bank Account<span style="color:red">*</span> :</label>
            <select name="bank" class="form-control" required>
                @foreach($bank_accounts as $bank_account)
                    <option value="{{$bank_account->id}}">{{$bank_account->bank_name}}</option>
                @endforeach
            </select>
        </div>

         <!-- <div class="form-group ">
                    <label>Service<span style="color:red">*</span> :</label>
                    <select name="service" id="service" class="form-control" required>
                    <option></option>
                    <option> .............select service..........</option>
                        @foreach($service as $item)

                            <option value="{{$item->id}}">{{$item->name}}</option>

                        @endforeach
                    </select>
                </div> -->
            <div class="form-group">
                    <input type="radio" class="form-check-input" name="type" value="all" id="allRadio" checked>
					<label for='allRadio' class="username" value="type">All</label>

                    <input type="radio" class="form-check-input" name="type" value="service" id='servRadio'>
                    <label for='servRadio' class="username"  value="service">Service</label>

                    <input type="radio" class="form-check-input" name="type" value="product" id='prodRadio'>
                    <label for='prodRadio' class="username" value="type">Product</label>
                </div>
 

       <!--  <div class="form-group">
            <label for="username">Purchase Type <span style="color:red">*</span> :</label>
            <select name="payment_type" class="form-control">

                    <option value="cash">Cash</option>
                    <option value="credit">Credit</option>

            </select>
        </div>
 -->


        <div class="form-actions form-group">

          <button type="submit" class="btn btn-primary btn-sm">Create</button>
        </div>

    </fieldset>
</form>


  </div>

</div>

@stop

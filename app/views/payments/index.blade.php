<?php
  function asMoney($value){
    return number_format($value, 2);
  }
?>

@extends('layouts.erp')
@section('content')

<div class="row">
  <div class="col-lg-12">
    <h4><font color="green">Payments</font></h4>
    <hr>
  </div>
</div>

<div class="col-lg-12">

  @if (Session::has('flash_message'))

    <div class="alert alert-success">
    {{ Session::get('flash_message') }}
   </div>
  @endif


  @if (Session::has('delete_message'))

    <div class="alert alert-danger">
    {{ Session::get('delete_message') }}
   </div>
  @endif
</div>



<div class="row">

  <div class="col-lg-12">

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#receivable">Receivable </a></li>
        <li><a data-toggle="tab" href="#payable">Payable </a></li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane fade in active" id="receivable">
          <div class="panel panel-default">
            <div class="panel-heading">
              <a href="{{ URL::to('payments/create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus fa-fw"></i> New Receivable Payment</a>&emsp;
              <a target="_blank" href="{{ URL::to('daily_payments/today') }}" class="btn btn-info btn-sm"> Daily Receivable Payment</a>&emsp;
              </div>
              <div class="panel-body">


          <table id="user2" class="table table-condensed table-bordered table-responsive table-hover">


            <thead>

              <th>#</th>
              <th>Client</th>
              <th>Amount</th>
               <th>Invoice_Number</th>
              <th>Status</th>
              <th>Date</th>
              <th></th>

            </thead>
            <tbody>

              <?php $i = 1; ?>
              @foreach($payments as $payment)
              @if($payment->client->type == 'Customer')
             <?php $invoice= Erporder::find($payment->erporder_id); ?>

              <tr>
                <td> {{ $i }}</td>

                <td>{{ $payment->client->name }}</td>

                <td>{{ asMoney($payment->amount_paid) }}</td>
                 <td>{{ $invoice->order_number }}</td>

                <!-- <td></td> -->
                @if($payment->is_approved==1)
                  <td>Approved</td>
                @elseif($payment->is_rejected==1)
                  <td>Rejected</td>
                @else
                  <td>Pending</td>
                @endif
                  <td>{{ date("d-M-Y",strtotime($payment->date)) }}</td>
                <td>
                        <div class="btn-group">
                        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          Action <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu" role="menu">
                          <li><a href="{{URL::to('payments/edit/'.$payment->id)}}">Update</a></li>

                          <li><a href="{{URL::to('payments/delete/'.$payment->id)}}"  onclick="return (confirm('Are you sure you want to delete this payment?'))">Delete</a></li>

                        </ul>
                    </div>

                          </td>



              </tr>

              <?php $i++; ?>
              @endif
              @endforeach


            </tbody>

                  


          </table>
        </div>


        </div>
        </div>
        <div class="tab-pane fade in" id="payable">
          <div class="panel panel-default">
            <div class="panel-heading">
              <a href="{{ URL::to('payments/payable/create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus fa-fw"></i> New Payable Payment</a>&emsp;

              <a href="{{ URL::to('daily_payables/today') }}" class="btn btn-info btn-sm"> Daily Payable Payment</a>&emsp;
              </div>
              <div class="panel-body">


          <table id="users" class="table table-condensed table-bordered table-responsive table-hover">


            <thead>

              <th>#</th>
              <th>Client</th>
              <th>Amount</th>
              <th>LPO No.</th>
              <th>Status</th>
              <th>Date</th>
              <th></th>

            </thead>
            <tbody>

              <?php $i = 1; ?>
              @foreach($payments as $payment)
              @if($payment->client->type == 'Supplier')
               <?php $lpo= Erporder::find($payment->erporder_id); ?>

              <tr>

                <td> {{ $i }}</td>


                <td>{{ $payment->client->name }}</td>

                <td>{{ asMoney($payment->amount_paid) }}</td>
                <td>{{ $lpo->order_number }}</td>
                <!-- <td></td> -->
                @if($payment->is_approved==1)
                  <td>Approved</td>
                @elseif($payment->is_rejected==1)
                  <td>Rejected</td>
                @else 
                  <td>Pending</td>
                @endif
                <td>{{ date("d-M-Y",strtotime($payment->date)) }}</td>
                <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Action <span class="caret"></span>
                  </button>
          
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{URL::to('payments/edit/'.$payment->id)}}">Update</a></li>
                   
                    <li><a href="{{URL::to('payments/delete/'.$payment->id)}}"  onclick="return (confirm('Are you sure you want to delete this payment?'))">Delete</a></li>
                    
                  </ul>
              </div>
              </td>

                </tr>
                <?php $i++ ?>
                @endif
                @endforeach
              </tbody>

          </table>
  </div><!-- ./End of body section -->

        </div>


        </div> <!-- End of Payable Tab -->

        </div>  <!-- End of all Tab content -->
      
      </div>


</div>

@stop

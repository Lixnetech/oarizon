<?php


function asMoney($value) {
  return number_format($value, 2);
}

?>

@extends('layouts.erp')
@section('content')



<div class="row">
	<div class="col-lg-12">
  <h4><font color='green'>Items</font></h4>

<hr>
</div>
</div>


<div class="row">
	<div class="col-lg-12">

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#items" aria-controls="remittance" role="tab"
                   data-toggle="tab">Items</a>
            </li>
            <li role="presentation">
                <a href="#items_category" aria-controls="profile" role="tab" data-toggle="tab">
                    Items Categories</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="items">
                  <div class="panel panel-default" style="margin-bottom: 10px;">
                    <div class="panel-heading">Items
                      @if (Session::has('flash_message'))

                        <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                       </div>
                      @endif

                      @if (Session::has('delete_message'))

                        <div class="alert alert-danger">
                        {{ Session::get('delete_message') }}
                       </div>
                      @endif

                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="btn btn-info btn-sm" href="{{ URL::to('items/create')}}">new item</a>
                          </div>
                          <div class="panel-body">


                      <table id="users" class="table table-condensed table-bordered table-responsive table-hover">


                        <thead>

                          <th>#</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Type</th>
                          <th>Category</th>
                          <th>Purchase Price</th>
                          <th>Selling Price</th>
                          <th></th>

                        </thead>
                        <tbody>

                          <?php $i = 1; ?>
                          @foreach($items as $item)

                          <tr>

                            <td> {{ $i }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->type }}</td>
                            <td>@if($item->category != null){{ $item->categoryname->name}}@endif</td>
                            <td align="right">{{ asMoney($item->purchase_price) }}</td>
                            <td align="right">{{ asMoney($item->selling_price) }}</td>
                            <td>

                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                      Action <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="{{URL::to('items/edit/'.$item->id)}}">Update</a></li>

                                      <li><a href="{{URL::to('items/delete/'.$item->id)}}" onclick="return (confirm('Are you sure you want to delete this item?'))">Delete</a></li>

                                    </ul>
                                </div>

                                      </td>



                          </tr>

                          <?php $i++; ?>
                          @endforeach


                        </tbody>


                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          <!-- Items category -->
            <div role="tabpanel" class="tab-pane" id="items_category">
                  <div class="panel panel-default" style="margin-bottom: 10px;">
                    <div class="panel-heading">Items Category
                      @if (Session::has('flash_message'))

                        <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                       </div>
                      @endif

                      @if (Session::has('delete_message'))

                        <div class="alert alert-danger">
                        {{ Session::get('delete_message') }}
                       </div>
                      @endif

                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="btn btn-info btn-sm" data-toggle="modal" data-target="#newcategory" >New category</a>
                          </div>
                          <div class="panel-body">


                      <table id="users" class="table table-condensed table-bordered table-responsive table-hover">


                        <thead>

                          <th>#</th>
                          <th>Name</th>
                          <th>created_at</th>
                          <th>updated_at</th>
                          <th></th>

                        </thead>
                        <tbody>

                          <?php $i = 1; ?>
                          @foreach($category as $category)

                          <tr>

                            <td> {{ $i }}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->created_at }}</td>
                            <td>{{ $category->updated_at }}</td>
                            <td>
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                      Action <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="{{URL::to('itemscategory/show/'.$category->id)}}">Show</a></li>
                                      <li><a href="{{URL::to('itemscategory/edit/'.$category->id)}}">Update</a></li>

                                      <li><a href="{{URL::to('itemscategory/delete/'.$category->id)}}" onclick="return (confirm('Are you sure you want to delete this item?'))">Delete</a></li>

                                    </ul>
                                </div>

                                      </td>



                          </tr>

                          <?php $i++; ?>
                          @endforeach


                        </tbody>


                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

    <div class="modal fade" id="newcategory" tabindex="-1" role="dialog" aria-labelledby="newcategory" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Create New Category</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="{{ URL::to('itemscategory') }}">
                <fieldset>
                  <div class="form-group">
                    <label for="name">Category name</label>
                    <input class="form-control" placeholder="" type="text" name="name" id="name" value=""></input>
                  </div>
                  <div class="form-group">
                    <button type="submit" type="btn btn-primary">Save</button>
                  </div>
                </fieldset>
              </form>
            </div>

          </div>
        </div>
      </div>


  </div>

</div>


@stop

@extends('layouts.portspay')
@section('content')

<div class="row">
    <div class="col-lg-12">
  <h3>Payroll Reports</h3>

<hr>
</div>  
</div>


<div class="row">
    <div class="col-lg-12">

    <table class="table  table-bordered table-hover table-condensed">
      
      <tr>
        <td>
          Monthly Payslips
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectPeriod') }}">Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>
      <tr>
        <td>
        Payroll Summary
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectSummaryPeriod') }}">Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>        
      </tr>
      @if(Entrust::can('manage_payrollinfo'))

      <tr>
        <td>
          Pay Remittance
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectRemittancePeriod') }}">Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>
      @endif
      <tr>
        <td>
          Earning Report
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectEarning') }}"> Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>
      
      <tr>
        <td>
          Overtime Report
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectOvertime') }}"> Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>
      
      <tr>
        <td>
          Allowance Report
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectAllowance') }}">Download <span class="glyphicon glyphicon-download-alt"></span></a>

        </td>
      </tr>
      
      <tr>
        <td>
          Non Taxable Income Report
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectnontaxableincome') }}">Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>
      
      <tr>
        <td>
          Pension Report
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectPension') }}" >Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>
      
      <tr>
        <td>
          Relief Report
        </td>
        <td>
          <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectRelief') }}"> Download <span class="glyphicon glyphicon-download-alt"></span></a>
        </td>
      </tr>

       <tr>
         <td>
           Deduction Report
         </td>
         <td>
           <a style="text-decoration: none;" href="{{ URL::to('payrollReports/selectDeduction') }}"> Download <span class="glyphicon glyphicon-download-alt"></span></a>    
         </td>
       </tr>

    </table>

  </div>

</div>

@stop
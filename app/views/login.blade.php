@include('includes.head')

<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <div class="login-panel panel panel-default">
                      
                    <div class="panel-body">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img src="{{asset('public/uploads/logo/oLtFm10n926Z.png')}}" alt="logo" width="50%">

                        <br>
               
                        <form role="form" method="POST" action="{{{ URL::to('/users/login') }}}" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                            <fieldset>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" tabindex="1" placeholder="Email" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                                </div>
                                <div class="form-group">
                                <label for="password">
                                    Password
                                </label>
                                <input class="form-control" tabindex="2" placeholder="Password" type="password" name="password" id="password">
                                <p class="help-block">
                                    <a href="{{{ URL::to('/users/forgot_password') }}}">(forgot_password)</a>
                                </p>
                                </div>
                                <div class="checkbox">
                                    <label for="remember">
                                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> Remember Me
                                    </label>
                                </div>
                                @if (Session::get('error'))
                                    <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
                                @endif
                        
                                @if (Session::get('notice'))
                                    <div class="alert">{{{ Session::get('notice') }}}</div>
                                @endif
                                <div class="form-group text-right">
                                    <button tabindex="3" type="submit" class="btn btn-primary">Login &nbsp;<i class="fa fa-sign-in" aria-hidden="true"></i></button>
                                </div>
                            </fieldset>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
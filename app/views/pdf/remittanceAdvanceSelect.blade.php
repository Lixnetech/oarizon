@extends('layouts.adv_ports')

@section('content')

<div class="row">
	<div class="col-lg-12">
  <h3>Select Period</h3>

<hr>
</div>	
</div>


<div class="row">
	<div class="col-lg-5">

    
		
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

		 <form target="_blank" method="POST" action="{{URL::to('advanceReports/advanceRemittances')}}" accept-charset="UTF-8">
   
    <fieldset>

        <div class="form-group">
                        <label for="username">Period <span style="color:red">*</span></label>
						 <select class="form-control selectable" name="period" id="period">
                            <option value="">Select Period</option>
                            <option value="day">As at date</option>
                            <option value="month">Month</option>
                            <option value="custom">Custom</option>
                            <option value="year">Year</option>
                    </select>
                    </div>
            <div class="form-group" id="select_date">

                <label for="username">Select Day <span style="color:red">*</span></label>
                <div class="right-inner-addon ">
                    <i class="glyphicon glyphicon-calendar"></i>
                    <input class="form-control datepicker" readonly="readonly" placeholder="" type="text"
                      name="day" id="date" value="{{date('Y-m-d')}}">
                </div>

                </div>

                <div class="form-group" id="month">
                          <label for="username">Select month <span style="color:red">*</span></label>
                          <div class="right-inner-addon ">
                               <i class="glyphicon glyphicon-calendar"></i>
                               <input class="form-control datepicker2" readonly="readonly" placeholder="" type="text"
                        name="month" id="date" value="{{date('m-Y')}}">
                           </div>
                </div>

                <div class="form-group" id="year">
                    <label for="year">Select Year <span style="color:red">*</span></label>
                    <div class="right-inner-addon ">
                    <i class="glyphicon glyphicon-calendar"></i>
                    <input required class="form-control datepicker42" type="text" readonly="readonly" placeholder="" name="year" value="{{date('Y')}}">
                </div>
                 </div>
                    <div class="form-group" id="custom">
                    <div class="form-group">
                        <label for="username">From <span style="color:red">*</span></label>
                        <div class="right-inner-addon ">
                            <i class="glyphicon glyphicon-calendar"></i>
                            <input required class="form-control datepicker" readonly="readonly" placeholder=""
                                   type="text" name="from" id="from" value="{{{ Input::old('from') }}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username">To <span style="color:red">*</span></label>
                        <div class="right-inner-addon ">
                            <i class="glyphicon glyphicon-calendar"></i>
                            <input required class="form-control datepicker" readonly="readonly" placeholder=""
                                   type="text" name="to" id="to" value="{{{ Input::old('to') }}}">
                        </div>
                    </div>
                    </div>
			

            <div class="form-group">
                        <label for="username">Select Branch: <span style="color:red">*</span></label>
                        <select required name="branch" class="form-control">
                            <option></option>
                            <option value='All'>All</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->id}}"> {{ $branch->name }}</option>
                            @endforeach

                        </select>
                
            </div>


            <div class="form-group">
                        <label for="username">Select Department: <span style="color:red">*</span></label>
                        <select required name="department" class="form-control">
                            <option></option>
                            <option value='All'>All</option>
                            @foreach($depts as $dept)
                            <option value="{{$dept->id}}"> {{ $dept->department_name }}</option>
                            @endforeach

                        </select>
                
            </div>


            <div class="form-group">
                        <label for="username">Download as: <span style="color:red">*</span></label>
                        <select required name="format" class="form-control">
                            <option></option>
                            <option value="excel"> Excel</option>
                            <option value="pdf"> PDF</option>
                        </select>
                
            </div>

        <div class="form-actions form-group">
        
          <button type="submit" class="btn btn-primary btn-sm">Select</button>
        </div>

    </fieldset>
</form>
		

  </div>

</div>


@stop
<html><head>
   <style>
   table{margin:0 auto;} 
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
    .balancediv{width:70%; border-radius:4px !important; padding:4px; 
        box-sizing:border-box; background-color:grey; margin:8px auto;}
   </style></head><body>
        <div class='onerow topdivrow'>
            <div>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}}<br>
                {{ $organization->email}}<br>
                {{ $organization->website}}<br>
                {{ $organization->address}}
            </div>
        </div><br>
        <div class='onerow headerrow'>
            <span><u>DEBIT NOTE</u></span> 
        </div>
        <div class='onerow headerrow'>
            <table> 
                <tr>
                    <td><b>Supplier Name<b></td> 
                    <td><b>Product<b></td> 
                    <td><b>Unit price<b></td>
                    <td><b>Quantity<b></td> 
                    <td><b>Balance<b></td>
                </tr> 
                <?php $order=Erporder::find($orderitem->erporder_id);  $client=Client::find($order->client_id);
                        $product=Item::find($orderitem->item_id);
                ?>
                <tr>
                    <td>{{$client->name}}</td> 
                    <td>{{$product->name}}</td>
                    <td>{{$product->purchase_price}}</td> 
                    <td>{{$orderitem->last_return}}</td>
                    <td>{{$product->purchase_price*$orderitem->last_return}}</td>
                </tr>
            </table>  <br>
            <div class="balancediv">
                    Balance <span>{{$product->purchase_price*$orderitem->last_return}}</span>
            </div>
            <div style="text-align:center;">
                <span> Prepared by {{Confide::user()->username}}  </span>
            </div>
        </div>
    </body></html>
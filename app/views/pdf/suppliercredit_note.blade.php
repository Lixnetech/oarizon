<html><head>
   <style>
   table{margin:0 auto;} 
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
    .balancediv,.clientdiv{width:70%; border-radius:4px !important; padding:4px; 
        box-sizing:border-box; background-color:grey; margin:8px auto;}
    .clientdiv{
       text-align:center; margin:0 auto;
    }
   </style></head><body>  
        <div class='onerow topdivrow'>
            <div>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}}<br>
                {{ $organization->email}}<br>
                {{ $organization->website}}<br>
                {{ $organization->address}}
            </div>
        </div><br>
        <div class='onerow headerrow'>
            <span><u>DEBIT NOTE</u></span> 
        </div>
        <div class='onerow headerrow'>
            <div class='clientdiv'>
                {{$client->name}}
            </div>  <br>
            <div class="balancediv">
                    Balance <span>{{$spurchases}}</span>
            </div>
            <div style="text-align:center;">
                <span> Prepared by {{Confide::user()->username}} </span>
            </div>
        </div>
    </body></html>
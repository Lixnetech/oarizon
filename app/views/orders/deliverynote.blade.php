<html ><head>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->

<style>

*{
  font-size: 16px ;
}

th,td{
  padding: 2px 7px !important;
}

div.mods{
  padding: 1% !important;
  width: 100% !important;
}

div.mods:nth-child(odd){
  margin-right: 1%;
  margin-left: 0%;
  background-color: #ffb6c1 !important;
}

div.mods:nth-child(even){
  margin-left: 1%;
  margin-right: 0%;
}

.page_break { page-break-before: always; }

@page { margin: 170px 20px; }
 .header { position: fixed; left: 0px; top: -150px; right: 0px; height: 150px;  text-align: center; }
 .content {margin-top: -120px; margin-bottom: -150px}
 .footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 50px;  }
 .footer .page:after { content: counter(page, upper-roman); }


  .demo {
    border:1px solid #C0C0C0;
    border-collapse:collapse;
    padding:0px;
  }
  .demo th {
    border:1px solid #C0C0C0;
    padding:0px;
  }
  .demo td {
    border:1px solid #C0C0C0;
    padding:3px;
  }


  .inv {
    border:1px solid #C0C0C0;
    border-collapse:collapse;
    padding:0px;
  }
  .inv th {
    border:1px solid #C0C0C0;
    padding:5px;
  }
  .inv td {
    border-bottom:0px solid #C0C0C0;
    border-right:1px solid #C0C0C0;
    padding:5px;
  }

img#watermark{
  position: fixed;
  width: 100%;
  z-index: 10;
  opacity: 0.1;
}

</style>


</head><body>

    <!-- <img src="{{ asset('public/uploads/logo/ADmzyppq2eza.png') }}" class="watermark"> -->
<div class="content">

<div class="row">
  <div class="col-lg-12">

  <?php

  $address = explode('/', $organization->address);

  ?>

      <table class="" style="border: 0px; width:100%">
               <tr class="logo">
    <td colspan="2"></td>
     <td colspan="2"></td>

      <td  style="width:150px">

            <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="logo" width="100%">
            

        </td>
          </tr>

          <tr>

            <!-- <td style="width:150px">

            <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="logo" width="50%">-->

        </td>

            <td >
            <strong>{{ strtoupper($organization->name.",")}}</strong><br><br>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            @for($i=0; $i< count($address); $i++)
            {{ strtoupper($address[$i])}}<br>
            @endfor


            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

            <td colspan="2" >
                <strong>Delivery Note</strong><br><br>
                <table class="demo" style="width:100%">
                  <br><br><br>
                  <tr >
                    <td><strong>Date</strong></td><td>{{ date('m/d/Y', strtotime($orderDetails['date']))}}</td>
                  </tr>
                  <tr>
                    <td><strong>Order No. #</strong></td><td>{{$orderDetails['delivery_number']}}</td>
                  </tr>

                </table>
            </td>
          </tr>



      </table>

      <br>
      <table class="demo" style="width:50%">
        <tr><?php $client = Client::find($orderDetails['client_id']); ?>
          <td><strong>{{$client->type}}</strong></td>
        </tr>
        <tr>
          <td>
            Name:&nbsp; <strong>{{$client->name}}</strong><br>
            Category:&nbsp; <strong>{{$client->category}}</strong><br>
            Contact Person:&nbsp; <strong>{{$client->contact_person}}</strong><br>
            Phone:&nbsp; <strong>{{$client->phone}}</strong><br>
            Email:&nbsp; <strong>{{$client->email}}</strong><br>
            Address:&nbsp; <strong>{{$client->address}}</strong><br>
          </td>
        </tr>
      </table>




      <br>

           <table class="inv" style="width:100%">

           <tr>
            <td style="border-bottom:1px solid #C0C0C0">Item name</td>
            <td style="border-bottom:1px solid #C0C0C0">Quantity</td>
            <!--<td style="border-bottom:1px solid #C0C0C0">Rate</td>
            <td style="border-bottom:1px solid #C0C0C0">Amount</td>-->
          </tr>


          @foreach($orderItems as $orderitem)


          <tr>
            <td >{{ Item::find($orderitem['item_id'])->name}}</td>
            <td>{{ $orderitem['quantity']}}</td>

          </tr>


      @endforeach

      </table>

  </div>
<br>
<i><b>Acknowledgement of goods received</b></i><br><br>

Received the above goods in good order and condition <br>
<br>
<p>1. Received by: .............................................Signature: ....................................... Date: ......................</p>
<p>2. Desk: &emsp;&emsp; <strong style="border-bottom: 1px solid dotted;">{{ Confide::user()->username }}</strong> &emsp;&emsp; Signature: ....................................... Date: &emsp;&emsp; <strong style="border-bottom: 1px solid dotted;">{{ date('d-m-Y') }}</strong></p>
<p>3. Delivery personell: ....................................................... Signature: ....................................... Date: &emsp;&emsp; <strong style="border-bottom: 1px solid dotted;">{{ date('d-m-Y') }}</strong></p>

</div>
</div>





<div class="footer">
  <p class="page">Page <?php $PAGE_NUM;  ?></p>
</div>

<br><br>




</body></html>

@extends('layouts.system')
@section('content')



<div class="row">

	<div class="col-lg-5">

      <form method="POST" action="{{{ URL::to('users/update/'.$user->id) }}}" accept-charset="UTF-8">
   
    <fieldset>
        <div class="form-group">
            <label for="username">{{{ Lang::get('confide::confide.username') }}}</label>
            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.username') }}}" type="text" name="username" id="username" value="{{ $user->username }}">
        </div>
   
        <div class="form-group">
            <label for="email">{{{ Lang::get('confide::confide.e_mail') }}} <small>{{ Lang::get('confide::confide.signup.confirmation_required') }}</small></label>
            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{ $user->email }}">
        </div>
        <div class="form-group">
            <label for="username">{{{ Lang::get('messages.firstname') }}}</label>
            <input class="form-control" placeholder="{{{ Lang::get('messages.firstname') }}}" type="text" name="first_name" id="first_name" value="{{ $user->first_name }}">
        </div>
                <div class="form-group">
            <label for="username">{{{ Lang::get('messages.lastname') }}}</label>
            <input class="form-control" placeholder="{{{ Lang::get('messages.lastname') }}}" type="text" name="last_name" id="last_name" value="{{ $user->last_name }}">
        </div>
             @if ( Entrust::can('manage_role') )
                   <div class="form-group">
                        <label for="username">User Role</label>
                        <select name="role_id" class="form-control">
                            
                            <option value=""><strong>Current Role:</strong>{{ $role->name }}</option>
                            @foreach($roles as $rolle)
                            <option value="{{ $rolle->id }}"> {{ $rolle->name }}</option>
                            @endforeach

                        </select>
                
                    </div>
                  @endif
        

        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif

        @if (Session::get('notice'))
            <div class="alert">{{ Session::get('notice') }}</div>
        @endif

        







        
      
        
        <div class="form-actions form-group">
        
          <button type="submit" class="btn btn-primary btn-sm">Update User</button>
        </div>

    </fieldset>
</form>
		

  </div>
</div>










@stop
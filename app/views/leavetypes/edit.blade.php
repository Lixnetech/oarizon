@extends('layouts.leave')
@section('content')
<style>
    .form-group{margin:12 auto; text-align:center !important; } 
    fieldset{width:100% !important; text-align:center !important; }
</style>

<div class="row">
	<div class="col-lg-12">
  <h3>Update Leave Type</h3>

<hr>
</div>	
</div>


<div class="row">
	<div class="col-lg-5">

    
		
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

		 <form method="POST" action="{{{ URL::to('leavetypes/update/'.$leavetype->id) }}}" accept-charset="UTF-8">
   
    <fieldset>
        <div class="form-group">
            <label for="username">Leave Type</label>
            <input class="form-control" placeholder="" type="text" name="name" id="name" value="{{ $leavetype->name}}">
        </div>


        <div class="form-group">
            <label for="username">Days Entitled</label>
            <input class="form-control" placeholder="" type="text" name="days" id="days" value="{{ $leavetype->days}}">
        </div>
        
        
        <div class="form-group">
            <label for="username">Include holidays</label>
            <input class="form-control" placeholder="" type="checkbox" name="in_holidays" id="in_holidays" value="">
        </div>

        <div class="form-group">
            <label for="username">Include weekends</label>
            <input class="form-control" placeholder="" type="checkbox" name="in_weekends" id="in_weekends" value="">
        </div>
        

        <div class="form-actions form-group">
        
          <button type="submit" class="btn btn-primary btn-sm">Update </button>
        </div>

    </fieldset>
</form>
		

  </div>

</div>

@stop
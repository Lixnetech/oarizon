<?php
	function asMoney($value){
		return number_format($value, 2);
	}
?>

@extends('layouts.accounting')
@section('content')

<div class="row">
	<div class="col-lg-12">
		<h4><font color="green">Asset categories</font></h4>
		<hr>
	</div>
</div>

<div class="row">
	<!-- QUICK LINK BUTTONS -->
	<div class="col-lg-12">
		<a href="{{ URL::to('assetCategory/create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus fa-fw"></i> New Asset Category</a>&emsp;
		<hr>
	</div><!-- ./END -->

	<!-- FIXED ASSETS BODY SECTION -->
	<div class="col-lg-12">
		<!-- TAB LINKS -->
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#registeredAssets">Categories ({{ AssetCategory::all()->count() }})</a></li>
			<!--<li><a data-toggle="tab" href="#soldDisposedAssets">Sold & Disposed ({{ Asset::where('status', '<>', 'new')->count() }})</a></li>-->
		</ul>

		<!-- TAB CONTENT -->
		<div class="tab-content"> 
			<!-- REGISTERED ASSETS -->
			<div id="registeredAssets" class="tab-pane fade in active">
				<table class="table table-condensed table-bordered table-responsive table-hover users">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead> 
					<tbody>
						<?php $count=1; ?>
						@if(count($assetCategories) > 0)
						@foreach($assetCategories as $assetCat)
						
						<tr>
							<td>{{ $count }}</td>
							<td>{{ $assetCat->name }}</td> 
							<td>{{ $assetCat->type }}</td>

							<td>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action <i class="fa fa-caret-down fa-fw"></i>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ URL::to('assetCategory/'.$assetCat->id.'/edit') }}">Edit</a></li>
										<li><a href="{{ URL::to('assetCategory/delete/'.$assetCat->id) }}" onclick="return (confirm('Are you sure you want to delete this item?'))">Delete</a></li>
									</ul>
								</div>
							</td>
						</tr>
						<?php $count++; ?>
						
						@endforeach  
						@endif
					
					</tbody>
				</table>
			</div><!-- ./End of registered assets -->

			<!-- SOLD/DISPOSED ASSETS -->
		</div><!-- ./End of tab cotent -->

	</div><!-- ./End of body section -->

</div>

@stop

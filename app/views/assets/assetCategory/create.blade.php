<?php
	function asMoney($value){
		return number_format($value, 2);
	}
?>

@extends('layouts.accounting')
@section('content')

<style type="text/css" media="screen">
	//hr{ border-color: #fff !important; }
</style>

<div class="row">
	<div class="col-lg-12">
		<h4><font color="green">New Asset Category</font></h4>
	</div>
</div>
@if ($errors->has())
	<div class="alert alert-danger">
		@foreach ($errors->all() as $error)
			{{ $error }}<br>
		@endforeach
	</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<form class="form-inline" role="form" action="{{ URL::to('assetCategory') }}" method="POST">
			<div class="form-group">
				<label>Category Name: </label><br>
				<input type="text" class="form-control input-sm" name="name" placeholder="Category Name" style="width: 300px" required>
			</div>&emsp;

			<div class="form-group">
				<label>Existing categories: </label><br>
				<select class="form-control input-sm" read-only name="nameList" id="catnamelist" style="width: 300px" >
					@foreach($assetCategories as $assetCat)
						<option value="{{$assetCat->id}}">{{$assetCat->name}}</option>
					@endforeach 
				</select>
			</div>

			<div class="form-group">
				<label>Type: </label><br>
				<input type="text" class="form-control input-sm" name="type" style="width: 250px" required>
			</div><br><br>

			<div class="col-lg-12 form-group text-right">
				<a href="{{ URL::to('assetCategory') }}" class="btn btn-danger btn-sm">Cancel</a>&emsp;
				<input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Create">
			</div><br><hr>

		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#rateRadio').on('click', function(){
			$('#rateRadio').prop('checked', true);
			$('#lifeYears').prop('checked', false);
			$('#rate').prop('disabled', false);
			$('#lifeYears').prop('disabled', true);
		});

		$('#rateYears').on('click', function(){
			$('#rateYears').prop('checked', true);
			$('#rateRadio').prop('checked', false);
			$('#lifeYears').prop('disabled', false);
			$('#rate').prop('disabled', true);
		});
		
	});
</script>

@stop

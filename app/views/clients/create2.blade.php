@extends('layouts.erp')
@section('content')

<div class="row">
	<div class="col-lg-12">
  <h4><font color='green'>New Suppliers</font></h4>

<hr>
</div>
</div>
<font color="red"><i>All fields marked with * are mandatory</i></font>

<div class="row">
	<div class="col-lg-5">



		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif

		 <form method="POST" action="{{{ URL::to('clients') }}}" accept-charset="UTF-8">

    <fieldset>
        <div class="form-group">
            <label for="username">Supplier Name <span style="color:red">*</span> :</label>
            <input class="form-control" placeholder="" type="text" name="name" id="name" value="{{{ Input::old('name') }}}" required>
        </div>

         <div class="form-group">
            <label for="username">Email:</label>
            <input class="form-control" placeholder="" data-parsley-trigger="focusout focusin" type="email" name="email_office" id="email_office" value="{{{Input::old('email_office') }}}" >
        </div>

        <div class="form-group">
            <label for="username">Phone:</label>
            <input class="form-control" placeholder="" data-parsley-trigger="change focusout" data-parsley-type="number" minlenght="10" type="text" name="office_phone" id="office_phone" value="{{{ Input::old('office_phone') }}}">
        </div>

        <div class="form-group">
            <label for="username">Address:</label>
            <input class="form-control" placeholder="" type="text" name="address" id="address" value="{{{ Input::old('email_personal') }}}">
        </div>

        <div class="form-group">
            <label for="username">Contact Name :</label>
            <input class="form-control" placeholder="" type="text" name="cname" id="cname" value="{{{ Input::old('cname') }}}">
        </div>

        <div class="form-group">
            <label for="username">Contact Personal Email:</label>
            <input class="form-control" placeholder=""data-parsley-trigger="focuout focusin" type="email" name="email_personal" id="email_personal" value="{{{ Input::old('email_personal') }}}">
        </div>

        <div class="form-group">
            <label for="username">Contact Personal Contact:</label>
            <input class="form-control" placeholder="" type="text" name="mobile_phone" id="mobile_phone" value="{{{ Input::old('address') }}}">
        </div>

				<!--<div class="form-group">
						<input type="hidden" class="form-check-input" name="type" value="Supplier">
						<label class="username" for="exampleCheck1" value="Supplier">Supplier</label>
				</div>-->
                <input type="hidden" name="type" value="Supplier">


        <div class="form-actions form-group">
          <button type="submit" class="btn btn-primary btn-sm">Create Supplier</button>
        </div>

    </fieldset>
</form>


  </div>

</div>
@stop

@extends('layouts.erp')
@section('content')
<style>
	.modal-body form{
		text-align:center;
	}
	.inwardspopbut,.outwardspopbut{ 
		cursor:pointer;
	}
	.collapse{position:relative;}
	.tableNav{
       display:; flex-direction:column;
    } 
	.tableNav section{margin-bottom:4px;}
	.tmainNav1{width:100%;} .tableHed{background-color:silver;}
	.tableHed div{font-size:14px; font-weight:bold;}
	.tableSection{display:flex; flex-direction:row; justify-content:space-between; width:100%; text-align:center;}
	.tableSection div{text-align:left;}
	.tmainSection1 div{width:16%;} .tjrSection1 div{width:12%;}  
	.collapseNav{
		background-color:silver; width:98%; margin:3px auto; padding:4px; box-sizing:border-box;
	}
</style>
<div class="row">
	<div class="col-lg-12">
  <h4><font color='green'>Clients</font></h4>
 
<hr>
</div>
</div>
<div class="row">
	<div class="col-lg-12">

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#clients" aria-controls="remittance" role="tab"
                   data-toggle="tab">Client</a>
            </li>
            <li role="presentation">
                <a href="#suppliers" aria-controls="profile" role="tab" data-toggle="tab">
                    Suppliers</a>
            </li> 
        </ul> 
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="clients">
                  <div class="panel panel-default" style="margin-bottom: 10px;">
                    <div class="panel-heading">
                      @if (Session::has('flash_message'))

                        <div class="alert alert-success">
                        	{{ Session::get('flash_message') }}
                       </div>
                      @endif

                      @if (Session::has('delete_message'))

                        <div class="alert alert-danger">
                        {{ Session::get('delete_message') }}
                       </div>
                      @endif
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#clientList" aria-controls="remittance" role="tab"
								data-toggle="tab">List</a>
							</li>
							<li role="presentation">
								<a href="#returnInwards" aria-controls="profile" role="tab" data-toggle="tab">
									Return inwards</a> 
							</li>
						</ul>
					</div>
					<div class="panel-body tab-content">
					<div role="tabpanel" class="tab-pane active" id="clientList">
						<div class='newclientdiv'>
							<a class="btn btn-info btn-sm" href="{{ URL::to('clients/create')}}">New Client</a>
						</div>
                      <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
						<thead>
							<th>#</th>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Address</th>
							<th>Type</th>
							<th></th>
						</thead>
						<tbody>

							<?php $i = 1; ?>
							@foreach($customers as $client)
								<?php $debt=Client::client_creditPurchases($client->id); ?>
							<tr> 
									@if($client->type =='Customer')
								<td> {{ $i }}</td>
								<td>{{ $client->name }}</td>
								<td>{{ $client->phone }}</td>
								<td>{{ $client->email }}</td>
								<td>{{ $client->address }}</td>
								<td>{{ $client->type }}</td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											Action <span class="caret"></span>
										</button>

										<ul class="dropdown-menu" role="menu">
											<li><a href="{{URL::to('clients/edit/'.$client->id)}}">Update</a></li>
											<li><a href="{{URL::to('clients/show/'.$client->id)}}">View Client</a></li>
											@if($debt>0) 
												<li><a href="{{URL::to('clients/clientdebit_note/'.$client->id)}}">Debit note</a></li>
											@endif 
											<li><a href="{{URL::to('clients/delete/'.$client->id)}}"  onclick="return (confirm('Are you sure you want to delete this client?'))">Delete</a></li>
										</ul>
									</div>
								</td> 

													@endif

							</tr>

							<?php $i++; ?>
							@endforeach


						</tbody>

                      </table>
                    </div>

					<div role="tabpanel" class="tab-pane" id="returnInwards"> 
                      <nav id="" class="tableNav tmainnav1">
					  		<section class='tableSection tmainSection1 tableHed'>
								<div>#</div>
								<div>Client name</div>
								<div>Order no.</div> 
								<div>No. of items</div>
								<div>Total</div>
								<div>Check Items</div>
							</section>
								<?php $i = 1; ?>
								@foreach($clientOrders as $clorder) 
									<?php 
										$clorder_items=Erporderitem::where("erporder_id",$clorder->id)->get();
										$iclient=Client::find($clorder->client_id);
										$total=Erporder::orderTotal($clorder->id);
										if($clorder->type=='invoice' || $clorder->payment_type=='credit') {
											$credit=1;
										}else{$credit=0;}
									?>
								<section class='tableSection tmainSection1'>
									<div>{{$i}}</div>
									<div>{{$iclient->name}}</div>
									<div>{{$clorder->order_number}}</div> 
									<div>{{count($clorder_items)}}</div>
									<div>{{$total}}</div>
									<div><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#colTable{{$i}}">Check items</button></div>
								</section>  	
								<nav id='colTable{{$i}}' class="collapse tableNav collapseNav" > <?php $i2=1; ?> 
									<section class="tableSection tjrSection1 tableHed">
										<div>#</div>
										<div>Name</div>
										<div>Item name</div> 
										<div>Unit price</div>
										<div>Quantity</div>
										<div>Total price</div>
										<div>Payment method</div>
										<div></div>
									</section>
									@foreach($clorder_items as $clorderItem)
										<?php 
											$item=Item::find($clorderItem->item_id);  $total=(int)$item->selling_price*(int)$clorderItem->quantity;
										?> 
									<section class="tableSection tjrSection1">
										<div> {{ $i2 }}</div>
										<div>{{ $iclient->name }}</div>
										<div>{{ $item->name }}</div>
										<div>{{ $item->selling_price }}</div> 
										<div>{{ $clorderItem->quantity }}</div>
										<div>{{ $total }}</div>
										<div>{{ $clorder->payment_type }}</div> 
										<div class="btn-group">
											<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												Action <span class="caret"></span> 
											</button>

											<ul class="dropdown-menu" role="menu">  
												<li class="inwardspopbut" data-toggle="modal" data-target="#inwardsModal" lang="{{$item->id}}" href="{{$clorderItem->id}}" src="{{ $clorder->payment_type }}" method="{{$credit}}"><a href='#'>Return</a></li>
												@if($credit==1)
													<li><a href="{{URL::to('clients/cdebit_note/'.$clorderItem->id)}}">Debit note</a></li>
												@else 
													<li><a href="{{URL::to('clients/ccredit_note/'.$clorderItem->id)}}">Credit note</a></li>
												@endif
											</ul>
										</div>  
									</section>
									<?php $i2++; ?>
								@endforeach 
								</nav>   
							<?php $i++; ?>
						@endforeach
                      </nav>
                    </div>

					</div>
              </div>
            </div>

<!--suppliers category-->
<div role="tabpanel" class="tab-pane" id="suppliers">
			<div class="panel panel-default" style="margin-bottom: 10px;">
				<div class="panel-heading">
					@if (Session::has('flash_message'))

						<div class="alert alert-success">
						{{ Session::get('flash_message') }}
					 </div>
					@endif

					@if (Session::has('delete_message'))

						<div class="alert alert-danger">
						{{ Session::get('delete_message') }}
					 </div>
					@endif
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#supplierList" aria-controls="remittance" role="tab"
							data-toggle="tab">List</a>
						</li>
						<li role="presentation">
							<a href="#returnOutwards" aria-controls="profile" role="tab" data-toggle="tab">
								Returns</a> 
						</li>
					</ul>
				</div>

				<div class="panel-body tab-content">
					<div role="tabpanel" class="tab-pane active" id="supplierList">
						<div class='newsupplierdiv'>
							<a class="btn btn-info btn-sm" href="{{ URL::to('create2')}}">New Supplier</a>		
						</div>
						<table id="users" class="table table-condensed table-bordered table-responsive table-hover">
							<thead>

								<th>#</th>
								<th>Name</th>
								<th>Phone</th>
								<th>Email</th>
								<th>Address</th>
								<th>Type</th>
								<th></th>

							</thead>
							<tbody>

								<?php $j = 1; ?>
								@foreach($suppliers as $client)
									<?php $debt2=Client::supplier_creditSales($client->id); ?>
								<tr>
										@if($client->type =='Supplier')
									<td> {{ $j }}</td>
									<td>{{ $client->name }}</td>
									<td>{{ $client->phone }}</td>
									<td>{{ $client->email }}</td>
									<td>{{ $client->address }}</td>
									<td>{{ $client->type }}</td>
									<td>

													<div class="btn-group">
													<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
														Action <span class="caret"></span>
													</button>

													<ul class="dropdown-menu" role="menu">
														<li><a href="{{URL::to('clients/edit/'.$client->id)}}">Update</a></li>
														<li><a href="{{URL::to('clients/show/'.$client->id)}}">View Station</a></li>
														@if($debt2>0)
															<li><a href="{{URL::to('clients/suppliercredit_note/'.$client->id)}}">Debit note</a></li>
														@endif  
														<li><a href="{{URL::to('clients/delete/'.$client->id)}}"  onclick="return (confirm('Are you sure you want to delete this supplier?'))">Delete</a></li>
													</ul>
											</div>

														</td>

														@endif

								</tr>

								<?php $j++; ?>
								@endforeach


							</tbody>

						</table>
					</div>

					
					<div role="tabpanel" class="tab-pane" id="returnOutwards"> 
                      <table id="users" class="table table-condensed table-bordered table-responsive table-hover">

							<thead>
								<th>#</th>
								<th>Supplier name</th>
								<th>Item name</th> 
								<th>Unit cost</th>
								<th>Quantity</th>
								<th>Total price</th>
								<th>Payment method</th>
								<th></th>
							</thead>
							<tbody>
								<?php $i = 1; ?>
								@foreach($companyOrders as $cmporder)
									<?php 
										$cmporder_items=Erporderitem::where("erporder_id",$cmporder->id)->get();
										$iclient=Client::find($clorder->client_id); 
										if($cmporder->payment_type=="credit"){
											$scredit=1;
										}else{$scredit=0;}
									?>
									@foreach($cmporder_items as $cmporderItem)
									<?php $item=Item::find($cmporderItem->item_id);  $total=(int)$item->purchase_price*(int)$cmporderItem->quantity;?>
								<tr>
									<td> {{ $i }}</td>
									<td>{{ $iclient->name }}</td>
									<td>{{ $item->name }}</td>
									<td>{{ $item->purchase_price }}</td>
									<td>{{ $cmporderItem->quantity }}</td>
									<td>{{ $total }}</td>
									<td>{{ $cmporder->payment_type }}</td> 
									<td>
										<div class="btn-group">
											<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												Action <span class="caret"></span>
											</button>
 
											<ul class="dropdown-menu" role="menu"> 
												<li class="outwardspopbut" data-toggle="modal" data-target="#outwardsModal" lang="{{$item->id}}" href="{{$cmporderItem->id}}" src="{{ $cmporder->payment_type }}" method="{{$scredit}}"><a>Return</a></li>
												@if($cmporder->payment_type=='credit')  
													<li><a href="{{URL::to('clients/scredit_note/'.$cmporderItem->id)}}">Credit note</a></li>
												@elseif($cmporder->payment_type=='cash')
													@if($cmporderItem->last_return>0) 
														<li><a href="{{URL::to('clients/sdebit_note/'.$cmporderItem->id)}}">Debit note</a></li>
													@endif 
												@endif  
											</ul>
										</div> 
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@endforeach

							</tbody>
                      </table>
                    </div>

				</div>

	</div>
</div>
</div>
</div>
</div>

<div id="inwardsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Return inwards quantity</h4>
      </div>
      <div class="modal-body">
		<form class="form-inline inwardsForm" role="form" action="{{ URL::to('erporders/returnInwards') }}" method="POST">
			<div class="form-group">
				<label>Quantity: </label><br>
				<input type="number" class="form-control input-sm" name="quantity" placeholder="" style="width: 300px" required>
				<input type="hidden" class="form-control inwardsitem_id" name="item_id" value="">
				<input type="hidden" class="form-control inwardsorder_id" name="erporder_id" value="">
				<input type="hidden" class="form-control payment_type" name="payment_type" value="">
				<input type="hidden" class="form-control increditInpu" name="incredit" value="">
			</div><br><br>
			<div class="form-group inpaymentSelect"> 
				<label>Payment by </label><br>
				<select name="pay_method" class="form-control input-sm" required>
					<?php $pmethods=Paymentmethod::all(); ?>
					@foreach($pmethods as $pmethod)
						<option value="{{$pmethod->id}}">{{$pmethod->name}}</option>
					@endforeach 
				</select>
			</div><br><br>
			<div class="form-group">
				<input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Return">
			</div>
		</form> 
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="outwardsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Return outwards quantity</h4>
      </div>
      <div class="modal-body">
		<form class="form-inline inwardsForm" role="form" action="{{ URL::to('erporders/returnOutwards') }}" method="POST">
			<div class="form-group">
				<label>Quantity: </label><br>
				<input type="number" class="form-control input-sm" name="quantity" placeholder="" style="width: 300px" required>
				<input type="hidden" class="form-control outwardsitem_id" name="item_id" value="">
				<input type="hidden" class="form-control outwardsorder_id" name="erporder_id" value="">
				<input type="hidden" class="form-control outpayment_type" name="payment_type" value="">
				<input type="hidden" class="form-control outcreditInpu" name="outcredit" value="">
			</div><br><br>
			<div class="form-group ">
				<label>Payment by </label><br>
				<select name="pay_method" class="form-control input-sm" required>
					<?php $pmethods=Paymentmethod::all(); ?>
					@foreach($pmethods as $pmethod)
						<option value="{{$pmethod->id}}">{{$pmethod->name}}</option>
					@endforeach 
				</select> 
			</div><br><br> 
			<div class="form-group">
				<input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Return">
			</div>
		</form> 
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div> 
</div>

<script type="text/javascript"> 
	$(document).ready(function(){
		$('.inwardspopbut').on('click', function(){
			var itemid=$(this).attr('lang');  var orderItem_id=$(this).attr('href');
			var payment_type=$(this).attr('src'); var credit=$(this).attr('method');
			$('.inwardsitem_id').val(itemid); $('.inwardsorder_id').val(orderItem_id); 
			$('.payment_type').val(payment_type);
			if(credit==1){$('.inpaymentSelect').hide(); $('.increditInpu').val('yes');}else{
				$('.increditInpu').val('no');  
			}
		});
		 
		$('.outwardspopbut').on('click', function(){
			var itemid=$(this).attr('lang');  var orderItem_id=$(this).attr('href');
			var payment_type=$(this).attr('src'); var credit=$(this).attr('method');
			$('.outwardsitem_id').val(itemid); $('.outwardsorder_id').val(orderItem_id); 
			$('.outpayment_type').val(payment_type);
			if(parseInt(credit)==1){$('.outpaymentSelect').hide(); $('.outcreditInpu').val('yes');}else{
				$('.outcreditInpu').val('no');   
			}
		}); 
	});
</script>
@stop

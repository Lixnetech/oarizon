@extends('layouts.accounting')

<script type="text/javascript">
 /**function totalBalance() {
      var instals = document.getElementById("instalments").value;
      var amt = document.getElementById("amount").value;
      var total = (instals * amt);
      document.getElementById("balance").value = total;
}**/

</script>

@section('content')

<br><div class="row">
  <div class="col-lg-12">
  <h4>Update Petty Cash Transaction</h4>

<hr>
</div>
</div>


<div class="row">
  <div class="col-lg-5">



     @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif

     <form method="POST" action="{{{ URL::to('petty_cash/update/'.$transaction->id) }}}" accept-charset="UTF-8" data-parsley-validate>

    <font color="red"><i>All fields marked with * are mandatory</i></font>
    <fieldset>



        <div class="form-group">
						<label>Voucher Number</label>
						<input type="text" class="form-control input-sm" readonly="readonly" name="voucher_number" id="voucher_number" placeholder="" value='{{$transaction->voucher_number}}' required>
					</div>
                       
					
					
					

					<div class="form-group">
						<label for="username">Date:</label>
		            <div class="right-inner-addon ">
		            	<i class="fa fa-calendar"></i>
		            	<input class="form-control input-sm datepicker21" readonly="readonly" type="text" name="tr_date" value="{{ date('Y-m-d') }}" required>
		            </div>
					</div>
                                  <div class="form-group">
						<label for="username">Requested Payment Date:<span style="color:red">*</span></label>
		            <div class="right-inner-addon ">
		            	<i class="fa fa-calendar"></i>
		            	<input class="form-control input-sm datepicker21"  type="text" name="rqpay_date" id="rqpay_date" value="{{ $transaction->req_payment_date}}" required>
		            </div>
					</div>
                         <div class="form-group">
						<label>Payee Name :<span style="color:red">*</span></label>
						<input type="text" class="form-control input-sm" name="payee_name" id="payee_name" value="{{ $transaction->payee_name}}" required>
					</div>
                        <div class="form-group">
						<label>Payee no :<span style="color:red">*</span></label>
						<input type="text" class="form-control input-sm" name="payee_no" id="payee_no" value="{{ $transaction->payee_no}}" required>
					</div>
                         <div class="form-group">
					<label>Quantity <span style="color:red">*</span>:</label><br>
					<input type="text" class="form-control input-sm" name="quantity" value="{{ $transaction->quantity}}" required>
				</div>&emsp;&nbsp;

				<div class="form-group">
					<label>Unit Price <span style="color:red">*</span>:</label><br>
					<input type="text" class="form-control input-sm" name="unit_price" value="{{ $transaction->unit_price}}" required>
				</div>&emsp;&nbsp;
                        
                    
                       @if(!empty($transaction->mpesa_code))                    
                    
                    <div class="form-group sub_pmethod1 hidden" id="mpesa" >
						<label>Mpesa Code :</label>
						<input type="text" class="for-control input-sm" name="mpesa_code" id="mpesa_code" placeholder="code" >
					</div>
					@endif
					@if(!empty($transaction->cheque_no))
                    <div class="form-group sub_pmethod2 hidden" id="cheque"> 
						<label>Cheque  No :</label> 
						<input type="text" class="form-control input-sm" name="cheque_no" id="cheque_no" placeholder="cheque number" >
					</div>
					@endif
					<div class="form-group">
						<label>Description (Transaction Description) <span style="color:red">*</span>:</label>
						<!--<textarea class="form-control input-sm" name="description" value="{{ $transaction->description}}" required></textarea>-->
						<input type="text" class="form-control input-sm" name="description" value="{{ $transaction->description}}" >
					</div>
                  
        <div class="form-actions form-group">

          <button type="submit" class="btn btn-primary btn-sm">Update Petty Cash</button>
        </div>

    </fieldset>
</form>


  </div>

</div>
<script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>
@stop

<?php
	function asMoney($value){
		return number_format($value, 2);
	}
?>

@extends('layouts.accounting')
@section('content')

<style type="text/css" media="screen">
	td.total{
		border-bottom: 3px double #777 !important;
		font-weight: bold !important;
	}	

</style>
<!-- MODAL WINDOW FOR COMMENT -->
<div id="commentModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="commentModalHeader"></h4>
            </div>
            <div class="modal-body">
                <form id="commentForm" role="form" action="{{URL::to('petty_cash/approve/'.$transid)}}" method="POST">
                     <!-- HIDDEN FIELDS -->
                    <input type="hidden" name="transid" value="{{$transid}}">

                    <div class="form-group">
                        <label>Enter Comment</label>
                        <textarea class="form-control" name="comment" id="comment" rows="4" placeholder="Enter comment here (OPTIONAL)"></textarea>
                    </div>
                    <hr>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button> &emsp; 
                        <button type="submit" id="submitBTN" class="btn btn-primary btn-sm">Submit</button>        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END COMMENT MODAL -->

<!-- MODAL WINDOW FOR COMMENT -->
<div id="rejectComment" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="commentModalHeader"></h4>
            </div>
            <div class="modal-body">
                <form id="commentForm" role="form" action="{{URL::to('petty_cash/reject/'.$transid)}}" method="POST">
                     <!-- HIDDEN FIELDS -->
                    <input type="hidden" name="transid" value="{{$transid}}">

                    <div class="form-group">
                        <label>Reason for  Rejection</label>
                        <textarea class="form-control" name="reject_comment" id="reject_comment" rows="4" placeholder="Why rejecting?"></textarea>
                    </div>
                    <hr>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button> &emsp; 
                        <button type="submit" id="submitBTN" class="btn btn-primary btn-sm">Submit</button>        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END COMMENT MODAL -->


<div class="row">
	<div class="col-lg-12">
		<h4><font color="green">Receipt Transactions</font></h4>
		<hr>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
        @if(Entrust::can('approve_pettycash') )

	@if($firstitem->is_approved==0 || $firstitem->is_rejected==1 )
        <a href="#commentModal" role="button" id="approvepetty" class="lnk btn btn-success btn-sm action_lnk" data-toggle="modal">
            <span class="glyphicon glyphicon-ok"></span>&nbsp; Approve
        </a>&emsp;
          @else
          @if($firstitem->is_approved==1 && $firstitem->is_rejected==0)
         <div class="panel"> 
          <h4><font color="Blue"> APPROVED  </font></h4>

            <div>
            @endif
         @endif
         
       @else
         @if($firstitem->is_approved==0 && $firstitem->is_rejected==0)

        <a href="{{URL::to('petty_cash/submitforapprova/'.$transid)}}" role="button" id="submitpetty" class="lnk btn btn-warning btn-sm action_lnk" >
            <span class="glyphicon glyphicon-send"></span>&nbsp; Submit For Approval
        </a>&emsp;
        @else
        @if($firstitem->is_approved==1 && $firstitem->is_rejected==0)
         <div class="panel">
          <h4><font color="green"> APPROVED {{$firstitem->approve_comment}}  </font></h4>

            <div>
            @endif
            @endif

        @endif



         @if(Entrust::can('reject_pettycash') )

    @if($firstitem->is_approved==0 && $firstitem->is_rejected==0)
        <a href="#rejectComment" role="button" id="reject_petty" class="lnk btn btn-warning btn-sm action_lnk" data-toggle="modal">
            <span class="glyphicon glyphicon-ok"></span>&nbsp; Reject
        </a>&emsp;
          @else
          @if($firstitem->is_rejected==1 && $firstitem->is_approved==0)
         <div class="panel">
          <h4><font color="red"> REJECTED {{$firstitem->reject_comment}} </font></h4>

            <div>
         @endif
          @endif
         
       @else
         @if($firstitem->is_rejected==1 && $firstitem->is_approved==0)

        <a href="{{URL::to('petty_cash/submitforapprova/'.$transid)}}" role="button" id="submitpetty" class="lnk btn btn-warning btn-sm action_lnk" >
            <span class="glyphicon glyphicon-send"></span>&nbsp; Submit For Approval
        </a>&emsp;
        @else
        @if($firstitem->is_rejected==1 && $firstitem->is_approved==0)
         <div class="panel">
          <h4><font color="red"> REJECTED {{$firstitem->reject_comment}} </font></h4>

            <div>
            @endif
            @endif

        @endif
		@if(count($items) > 0)
			<table class="table table-condensed table-bordered table-responsive table-hover">
				<thead>
					<th>#</th>
					<th>Item</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit Price</th>
					<th>Total Amount</th>
				</thead>
				<tbody>
					<?php $count = 0; $itemTotal = 0; $grandTotal = 0; ?>
					@foreach($items as $trItem)
						<?php 
							$itemTotal = $trItem['quantity'] * $trItem['unit_price']; 
							$grandTotal += $itemTotal;
						?>
						<tr>
							<td>{{ $count+1 }}</td>
							<td>{{ $trItem['item_name'] }}</td>
							<td>{{ $trItem['description'] }}</td>
							<td>{{ $trItem['quantity'] }}</td>
							<td>{{ $trItem['unit_price'] }}</td>
							<td>{{ $itemTotal }}</td>
                                                       <td> @if($firstitem->is_approved==0)
                                                 <a href="{{ URL::to('petty_cash/edit/'.$trItem['id'])}}" class="btn btn-warning btn-sm" >Edit Item</a>
							@else
							<a href="{{ URL::to('petty_cash/edit/'.$trItem['id'])}}" class="btn btn-warning btn-sm disabled" >Edit Item</a>
							@endif
                                                     </td>
						</tr>
                                                
						<?php $count++; ?>
					@endforeach
					<tr>
						<td colspan="4"></td>
						<td class="total">Grand Total</td>
						<td class="total">{{ $grandTotal }}</td>
					</tr>
				</tbody>
			</table>
                   <div>
                                                 @if($firstitem->is_approved==1)
                                                 <a href="{{ URL::to('petty_cash/voucher/'.$transid)}}" class="btn btn-warning btn-sm" >Generate a Voucher</a>
							@else
							<a href="{{ URL::to('petty_cash/voucher/'.$transid)}}" class="btn btn-warning btn-sm disabled" >Generate a Voucher</a>
							@endif
                                       </div>
		@else
			<h4><font color="red">This is a single transaction. No receipt</font></h4>
		@endif
	</div>
</div>

@stop
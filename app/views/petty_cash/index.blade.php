<?php
	function asMoney($value) {
	  return number_format($value, 2);
	}
?>

@extends('layouts.accounting')
@section('content')

<style>
	
	.top-header{
		background: #E1F5FE !important;
		color: #777;
		vertical-align: middle !important;
		padding: 10px 5px !important;
		text-align: center;
	}

	div.head{
		display: inline-block;
		width: 49%;
		margin-right: 0px;
		padding: 0 !important;
	}

	div.left{
		display: inline-block;
		text-align: center;
		border-right: 1px solid #ccc !important;
	}

	h5{
		margin: 4px 10px 5px 10px !important;
	}

	h6{
		margin: 5px 10px 4px 10px !important;
	}
	.hidden{display:none;}

</style>
 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif
<!-- CHECK IF A PETTY CASH ACCOUNT EXISTS, ELSE CREATE ONE -->
@if(count($petty_account) > 0)

<!-- MODAL WINDOW FOR COMMENT-->
<div id="amountModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="commentModalHeader"></h4>
            </div>
            <div class="modal-body">
                <form id="commentForm" role="form" action="{{URL::to('petty_cash/submit_request')}}" method="POST">
                     <!-- HIDDEN FIELDS -->

                    <div class="form-group">
                        <label>Amount Requested</label>
                        <input type="text" class="form-control input-sm" name="amount" placeholder="" value='{{$petty_account->balance}}' required>
                    </div>
                    <hr>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button> &emsp; 
                        <button type="submit" id="submitBTN" class="btn btn-primary btn-sm">Submit</button>        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END COMMENT MODAL -->



<!-- ADD MONEY TO PETTY CASH FROM ASSET ACCOUNT -->
<div id="receiveMoney" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ URL::to('petty_cash/addMoney') }}" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title"><font color="green">Transfer Funds to Petty Cash Account</font></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>From:</label>
						<select class="form-control input-sm" name="ac_from" required>
							<option value="">--- Select an account ---</option>
							@if(count($assets) > 0)
                                                        
							@foreach($assets as $asset)
                             @if($asset->name !=$petty_account->name)
								<option value="{{ $asset->id }}">{{ $asset->code }} - {{ $asset->name }} - (Balance: KES. {{ $asset->balance }})</option>
                               @endif

							@endforeach
							@endif
						</select>
					</div>

					<div class="form-group">
						<label>To (Petty Cash): </label>
						<select class="form-control input-sm" name="ac_to" required>
							<!--<option value="">--- Select an account ---</option>-->
							@if(count($assets) > 0)
							@foreach($assets as $asset)
                            @if($asset->name==$petty_account->name)
								<option value="{{ $asset->id }}">{{ $asset->code }} - {{ $asset->name }}</option>
                            @endif

							@endforeach
							@endif
						</select> 
					</div>
					<div class="form-group">
			         <label>Petty cash balance:</label> 
			         <div class="input-group"> 
					 	<span class="input-group-addon">KES</span>
			            <input type="text" class="form-control input-sm" readonly="readonly" name="cash_in_hand" value='{{$petty_account->balance}}'>
			         </div>
					</div> 
					<div class="form-group">
			         <label>Petty Cash in hand:</label> 
			         <div class="input-group">
					 	<span class="input-group-addon">KES</span> 
			            <input type="text" class="form-control input-sm" name="cash_in_hand" placeholder="" value='{{$petty_account->balance}}' required>
			         </div>
					</div>
					<div class="form-group">
			         <label>Transfer Amount:</label> 
			         <div class="input-group">
			            <span class="input-group-addon">KES</span>
			            <input type="text" class="form-control input-sm" name="amount" placeholder="{{ asMoney(0) }}" required>
			         </div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>&emsp;
					<input type="submit" class="btn btn-primary btn-sm" value="Receive Money">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ./END -->
<!-- ========================================================================== -->


<!-- ADD MONEY TO PETTY CASH FROM OWNER'S CONTRIBUTION ACCOUNT -->
<div id="receiveContribution" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ URL::to('petty_cash/addContribution') }}" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title"><font color="green">Owner's Contribution to Petty Cash</font></h4>
					<p><font color="red">Please create owner's contribution account first as a liability.</font></p>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>From:</label>
						<select class="form-control input-sm" name="cont_acFrom" required>
							<option value="">--- Select an account ---</option>
							@if(count($liabilities) > 0)
							@foreach($liabilities as $liability)
								<option value="{{ $liability->id }}">{{ $liability->code }} - {{ $liability->name }}</option>
							@endforeach
							@endif
						</select>
					</div>

					<div class="form-group">
						<label>To:</label>
						<select class="form-control input-sm" name="cont_acTo" required>
							<option value="">--- Select an account ---</option>
							@if(count($assets) > 0)
							@foreach($assets as $asset)								<option value="{{ $asset->id }}">{{ $asset->code }} - {{ $asset->name }}</option>
							@endforeach
							@endif
						</select>
					</div>
					
					<div class="form-group">
						<label>Contributor's Name:</label>
						<input type="text" name="cont_name" class="form-control input-sm" placeholder="Contributor's Name" required>
					</div>

					<div class="form-group">
			         <label>Transfer Amount:</label> 
			         <div class="input-group">
			            <span class="input-group-addon">KES</span>
			            <input type="text" class="form-control input-sm" name="cont_amount" placeholder="{{ asMoney(0) }}" required>
			         </div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>&emsp;
					<input type="submit" class="btn btn-primary btn-sm" value="Receive Money">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ./END -->


<!-- NEW PETTY CASH TRANSACTION -->
<div id="newTransaction" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ URL::to('petty_cash/newTransaction') }}" method="POST">
				<input type="hidden" name="credit_ac" value="{{ $petty_account->id }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title"><font color="green">New Transaction - Receipt Details</font></h4>
				</div>
				<div class="modal-body">
                                    <div class="form-group">
						<label>Voucher Number</label>
						<input type="text" class="form-control input-sm" readonly="readonly" name="voucher_number" id="voucher_number" placeholder="" value='{{$voucher_number}}' required>
					</div>
                       
					<div class="form-group">
						<label>Receipt from (Where the Money was used) :</label>
						
                                            <select class="form-control input-sm" name="transact_to" required>
							@if(count($assets) > 0)
							@foreach($expenses as $expense)
								@if($expense->name==$petty_expense->name)
								<option value="{{ $expense->id }}">{{ $expense->code }} - {{ $expense->name }}</option>
                                                                @endif
							@endforeach
							@endif
						</select>

					</div>
					
					<div class="form-group">
						<label>Spending Account (Petty Cash) <span style="color:red">*</span> :</label>
						<select class="form-control input-sm" name="expense_ac" required>
							<!--<option value="">--- Select an account ---</option>-->
							@if(count($assets) > 0)
							@foreach($assets as $asset)
						   @if($asset->name==$petty_account->name)
								<option value="{{ $asset->id }}">{{ $asset->code }} - {{ $asset->name }}</option>
                            @endif
							@endforeach
							@endif
						</select>
					</div>

					<div class="form-group">
						<label for="username">Date:</label>
		            <div class="right-inner-addon ">
		            	<i class="fa fa-calendar"></i>
		            	<input class="form-control input-sm datepicker21" readonly="readonly" type="text" name="tr_date" value="{{ date('Y-m-d') }}" required>
		            </div>
					</div>
                                  <div class="form-group">
						<label for="username">Requested Payment Date:</label>
		            <div class="right-inner-addon ">
		            	<i class="fa fa-calendar"></i>
		            	<input class="form-control input-sm datepicker21"  type="text" name="rqpay_date" id="rqpay_date" value="{{ date('Y-m-d') }}" required>
		            </div>
					</div>
                         <div class="form-group">
						<label>Payee Name :</label>
						<input type="text" class="form-control input-sm" name="payee_name" id="payee_name" placeholder="name" required>
					</div>
                        <div class="form-group">
						<label>Payee no :</label>
						<input type="text" class="form-control input-sm" name="payee_no" id="payee_no placeholder="phone number" required>
					</div>
                                                 
                    <div class="form-group">
						<label>Station<span style="color:red">*</span> :</label>
						<select class="form-control input-sm" name="station_id" required>
							<option value="">--- Select  station ---</option>
		                                  @foreach($stations as $station)
								<option value="{{ $station->id }}">{{ $station->station_name }}</option>
							@endforeach
						</select>
					</div>
                                           
                    <div class="form-group">
						<label>Payment Method<span style="color:red">*</span> :</label>
						<select class="form-control input-sm pmethod" name="pay_method" id="pay_method" required>
							<option value="">--- Select  Payment Method ---</option>
		                    @foreach($methods as $method)
							<option value="{{ $method->name }}">{{ $method->name }}</option>
							@endforeach
						</select>
					</div>
                    <div class="form-group sub_pmethod1 hidden" id="mpesa" >
						<label>Mpesa Code :</label>
						<input type="text" class="for-control input-sm" name="mpesa_code" id="mpesa_code" placeholder="code" >
					</div>
                    <div class="form-group sub_pmethod2 hidden" id="cheque"> 
						<label>Cheque  No :</label> 
						<input type="text" class="form-control input-sm" name="cheque_no" id="cheque_no" placeholder="cheque number" >
					</div>
					<div class="form-group">
						<label>Description (Transaction Description) <span style="color:red">*</span>:</label>
						<textarea class="form-control input-sm" name="description" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>&emsp;
					<input type="submit" class="btn btn-primary btn-sm" value="Create">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ./END -->



<div class="row">
	<div class="col-lg-12">
		<h4><font color="green">Petty Cash</font></h4>
		<hr>
	</div>
</div>

<!-- ERROR MESSAGE -->
@if(Session::has('error'))
<div class="alert alert-danger fade in">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Error! </strong>&emsp;{{ Session::get('error') }}<br>
   {{ Session::forget('error') }}
</div>
@endif

<!-- SUCCESS MESSAGE -->
@if(Session::has('success'))
<div class="alert alert-success fade in">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Success! </strong>&emsp;{{ Session::get('success') }}<br>
   {{ Session::forget('success') }}
</div>
@endif


<div class="row">
	<!-- HEADER INFO -->
	<div class="col-lg-12">
		<div class="top-header">
			<div class="head text-left"> 
				<div class="left">
					<h5><font color="#0BAEED">Petty Cash</font></h5>
					<h6>Account</h6>
				</div>
				<div class="left">
					<h5><font color="#0BAEED">Account Balance</font></h5>
					<h6>KES: {{ asMoney($petty_account->balance) }}</h6>
				</div>
			</div>
			<div class="head text-left">
				<div class="right">
					<div class="pull-right">
                                                 @if (Entrust::can('transfer_to_petty'))
						<a href="#receiveMoney" class="btn btn-success btn-sm" data-toggle="modal">Transfer From</a></li>&emsp;
                                                                                                  
                                                @else
                                            <a href="#amountModal" class="btn btn-info btn-sm" data-toggle="modal">Request Reimbursement</a></li>&emsp;                                                     
                                                @endif
						
						<a href="#newTransaction" class="btn btn-warning btn-sm" data-toggle="modal"><i class="fa fa-plus fa-fw"></i>New Transaction</a></li>
	            </div>
				</div>
			</div>
		</div><hr>
	</div>

	<!-- BODY INFO -->
	<div class="col-lg-12">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#reconcile">Account Transactions</a></li>
		</ul>

		<div class="tab-content">
			<div id="reconcile" class="tab-pane fade in active">
				<table  class="table table-condensed table-bordered table-responsive table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Date</th>
							<th>Description</th>
							<th>Status</th>
							<th>Spent</th>
							<th>Received</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php $count=1; ?>
						@if(count($ac_transactions) > 0)
							@foreach($ac_transactions as $transaction)
							<tr>
						<?php $item = PettycashItem::where('ac_trns', $transaction->id)->first();?>
								@if(!empty($item))
								<td>{{ $count }}</td>
								<td>{{ $transaction->transaction_date }}</td>
								<td>{{ $transaction->description }}</td>
								@if($item->is_approved==1)
									<td>APPROVED</td>
								@elseif($item->is_rejected==1)
									<td>REJECTED</td>
                                                                @else
									<td>NOT APPROVED</td>

								@endif
								
								@if($transaction->account_credited == $petty_account->id)
									<td>{{ $transaction->transaction_amount }}</td>
									<td></td>
								@elseif($transaction->account_debited == $petty_account->id)
									<td></td>
									<td>{{ $transaction->transaction_amount }}</td>
								@endif
							<td> 
								<!--<div class="btn-group">
			                  <a href="{{URL::to('petty_cash/transaction/'.$transaction->id)}}" class="btn btn-info btn-sm">View</a>
			               </div>-->
                     <div class="btn-group">
                  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Action <span class="caret"></span>
                  </button>
          
                  <ul class="dropdown-menu" role="menu">
                   
                    <li><a href="{{URL::to('petty_cash/transaction/'.$transaction->id)}}">View</a></li>
                    <!--<li><a href="{{URL::to('petty_cash/update/'.$transaction->id)}}">Update</a></li>-->
                   
                    
                  </ul>
              </div>



							</td>
                                                       @endif  
						</tr>
						<?php $count++; ?>
						@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

@else
<div class="row">
	<div class="col-lg-12">
		<h4><font color="red">NO PETTY CASH ACCOUNT AVAILABLE PLEASE CREATE ONE!!! (AS AN ASSET ACCOUNT)</font></h4>
	</div>
	<div class="col-lg-12">
		<a href="{{ URL::to('accounts/create') }}" class="btn btn-primary btn-sm">Create Account</a>
	</div>
</div>
@endif

<script type="text/javascript">
 $(document).ready(function(){ 
	$('.sub_pmethod2').removeClass('hidden'); $('.sub_pmethod2').fadeOut();
	$('.sub_pmethod1').removeClass('hidden'); $('.sub_pmethod1').fadeOut();
	$('.pmethod').change(function(){
		var mainVal=$(this).val(); 
		if(mainVal=="Cheque"){
			$('.sub_pmethod2').fadeIn(200); $('.sub_pmethod1').fadeOut(100); 
		}else if(mainVal=="Mobile money"){
			$('.sub_pmethod1').fadeIn(200); $('.sub_pmethod2').fadeOut(100); 
		}else{$('.sub_pmethod1').fadeOut(200); $('.sub_pmethod2').fadeOut(100); }
	});
	}); 
</script> 

@stop
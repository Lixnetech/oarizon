<html ><head>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->

<style>

*{
  font-size: 16px ;
}

th,td{
  padding: 2px 7px !important;
}

div.mods{
  padding: 1% !important;
  width: 100% !important;
}

div.mods:nth-child(odd){
  margin-right: 1%;
  margin-left: 0%;
  background-color: #ffb6c1 !important;
}

div.mods:nth-child(even){
  margin-left: 1%;
  margin-right: 0%;
}

.page_break { page-break-before: always; }
.payDiv{text-align:center; width:100%; padding:5px 2px; box-sizing:border-box; }
.payDiv span{margin:3px;}
.transtable{margin:0 auto; text-align:center;}

@page { margin: 200px 100px; }
 .header { position: fixed; left: 0px; top: -150px; right: 0px; height: 150px;  text-align: center; }
 .content {margin-top: -120px; margin-bottom: -150px}
 .footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 50px;  }
 .footer .page:after { content: counter(page, upper-roman); }


  .demo {
    border:1px solid #C0C0C0;
    border-collapse:collapse;
    padding:0px;
  }
  .demo th {
    border:1px solid #C0C0C0;
    padding:0px;
  }
  .demo td {
    border:1px solid #C0C0C0;
    padding:3px;
  }


  .inv {
    border:1px solid #C0C0C0;
    border-collapse:collapse;
    padding:0px;
  }
  .inv th {
    border:1px solid #C0C0C0;
    padding:5px;
  }
  .inv td {
    border-bottom:0px solid #C0C0C0;
    border-right:1px solid #C0C0C0;
    padding:5px;
  }

img#watermark{
  position: fixed;
  width: 100%;
  z-index: 10;
  opacity: 0.1;
}
td.total{
		border-bottom: 3px double #777 !important;
		font-weight: bold !important;
	}	

</style>


</head><body>

    <!-- <img src="{{ asset('public/uploads/logo/ADmzyppq2eza.png') }}" class="watermark"> -->
<div class="content">

<div class="row">
  <div class="col-lg-12">

  <?php

  $address = explode('/', $organization->address);

  ?>

      <table class="" style="border: 0px; width:100%">
               <tr class="logo">
    <!-- <td colspan="2"></td> -->
     <!--<td colspan="2"></td> -->
     <td colspan="2"></td>

      <td  style="width:150px" align="center">

            <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="logo" width="100%">
            

        </td>
        <td colspan="2"></td>
        <td>
        <table class="" style="border: 0px; width:100%">
        <tr>
        <td style="border:3px solid #C0C0C0;width:20px;color:red " align="center"><strong>{{{$firstitem->voucher_number}}}</strong></td>
        <td colspan="2"></td>
        </tr>
        </table> 
        </td>
          </tr>
         <tr> 
          <td colspan="2"></td>
          <td colspan="2"><strong>{{ strtoupper($organization->name)}}.</strong><br><br></td></tr>
          <tr>

            <!-- <td style="width:150px">

            <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="logo" width="50%">-->

        </td>
          
            <td colspan="2">
            <!--<strong>{{ strtoupper($organization->name.",")}}</strong><br><br>-->
            
            
            @for($i=0; $i< count($address); $i++)
            {{ strtoupper($address[$i])}}<br>
            @endfor


            </td>
            <td>&nbsp;</td>

            <td ></td>
            <td colspan="2">{{ $organization->email}}<br>
                 {{ $organization->website}}<br>
                {{ $organization->phone}}<br>
              </td>
              
            
          </tr>
           <tr>
           <td colspan="2"></td>
           <td colspan="2" style="border:2px solid #C0C0C0;color:blue">PAYMENT VOUCHER</td>
           <td colspan="1"></td>
           
           <td colspan="1"></td>
             </tr>
            


      </table>
    <br>
     <table class="" style="border: 0px; width:100%">
               <tr class="logo">
    <td colspan="6"><p><strong>PAYEE NAME:</strong>&nbsp;&nbsp;<u>{{$firstitem->payee_name }}</u></td>
     <td colspan="6"><p><strong> DATE:</strong> &nbsp;&nbsp;<u> </u><strong style="border-bottom: 1px solid ;">{{ date('d-m-Y') }}</strong></td>

      <td  style="width:150px">

                        

        </td>
          </tr>

          <tr>

            <td colspan="6">
            <p ><strong>Payee Phone no:</strong>&nbsp;&nbsp;<u>{{ $firstitem->payee_no}}</u>  </p >

            </td>
             
            <td colspan="6"><p><strong>Station:</strong>&nbsp;&nbsp;<u>{{$station->station_name}}</u></p ></td>
            
          </tr>
            <tr>
            <td colspan="6" >

           <p ><strong> REQUESTED PAYMENT DATE:</strong>&nbsp;&nbsp;<u>{{ $firstitem->req_payment_date}}</u></p>
            </td >
             <td colspan="6" ></td>
          </tr>



      </table>
              <br><br>


                      @if(count($items) > 0)

                   <table class="table table-condensed table-bordered table-responsive table-hover transtable" style="border: 1px solid ;">
                    <tr>

                       <td colspan="4"><p><strong>TRANSACTION DETAILS<!--PURPOSE AND DETAILS OF PAYMENT--></strong></p></td>
                       </tr>
                        <tr>

                       <td colspan="4" ><br></td>
                       </tr>


				<tr>
					<th align="center">#</th>
					<th align="center">Item</th>
					<!--<th>Description</th> -->

					<th align="center">Quantity</th>
					<th align="center">Unit Price</th>
                                        <th align="center"></th>
					<th align="center">Total Amount</th>
				</tr>
									<?php $count = 0; $itemTotal = 0; $grandTotal = 0; ?>
					@foreach($items as $trItem)
						<?php 
							$itemTotal = $trItem['quantity'] * $trItem['unit_price']; 
							$grandTotal += $itemTotal;
						?>
						<tr>
							<td align="center">{{ $count+1 }}</td>
							<td align="center">{{ $trItem['item_name'] }}</td>
							<!--<td>{{ $trItem['description'] }}</td> -->
							<td align="center">{{ $trItem['quantity'] }}</td>
							<td align="center">{{ $trItem['unit_price'] }}</td>
                                                         <td align="center"></td>
							<td align="center">{{ $itemTotal }}</td>
						</tr>
						<?php $count++; ?>
					@endforeach
					<tr>
						<td colspan="4"></td>
						<td class="total" align="center">Grand Total</td>
						<td class="total" align="center">{{ $grandTotal }}</td>
					</tr>
				
			</table> 
			<br>

      <?php $pay_method=$trans->transaction_form; $pay_details=$trans->form_details;
          if(!empty($pay_method)){
      ?>
      <div class='payDiv'>
          <span><u><b>MODE OF PAYMENT</b></u></span><br>
        @if($pay_method=='Mobile money')
            <span>Mpesa</span> || <span>Mpesa code:: {{$pay_details}}</span>
        @elseif($pay_method=='Cheque')
            <span>Cheque</span> || <span>Cheque no:: {{$pay_details}}</span>
        @endif
          <!--<span>Mpesa</span> || <span>Mpesa code::09305897</span>-->
      </div> 
      <?php } ?>
        
@else
			<h4><font color="red">No Transactions for this voucher</font></h4>
		@endif


  <br>
<p>1. Prepared by: &emsp;&emsp;....... <strong style="border-bottom: 1px solid dotted;">{{ Confide::user()->first_name }}&nbsp; {{ Confide::user()->last_name }}</strong> &emsp;&emsp;.......Sign: ....................................... Date:&emsp;&emsp; <strong style="border-bottom: 1px solid dotted;">{{ date('d-m-Y',strtotime($firstitem->transaction_date)) }}</strong> </p>
<p>2. Approved by: &emsp;&emsp;....... <strong style="border-bottom: 1px solid dotted;">{{ $firstitem->approved_by }}</strong> &emsp;&emsp; .......Sign: ....................................... Date: &emsp;&emsp; <strong style="border-bottom: 1px solid dotted;">{{ date('d-m-Y',strtotime($firstitem->approval_date)) }}</strong></p>
<p>3. Received by: &emsp;&emsp;....... <strong style="border-bottom: 1px solid dotted;"> {{$firstitem->payee_name }}</strong> &emsp;&emsp; .......Sign: ....................................... Date: &emsp;&emsp; <strong style="border-bottom: 1px solid dotted;">{{ date('d-m-Y') }}</strong></p>

</div>
</div>





<div class="footer">
  <p class="page">Page <?php $PAGE_NUM;  ?></p>
</div>

<br><br>




</body></html>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Lixnet Financials </title>
    <link rel="shortcut icon" href="#" />

{{--     <img src="{{asset('images/ourlogo.png')}}">--}}

    <!-- Core CSS - Include with every page -->

    <link rel="stylesheet" type="text/css" href="{{asset('jquery-ui-1.11.4.custom/jquery-ui.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('font-awesome/css/font-awesome.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/sb-admin.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/sb-parsley.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('media/css/jquery.dataTables.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('datepicker/css/bootstrap-datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400"/>

    {{-- {{ HTML::style('jquery-ui-1.11.4.custom/jquery-ui.css') }}

    {{ HTML::style('css/bootstrap.min.css') }}
    
   
   {{ HTML::style('font-awesome/css/font-awesome.css') }} --}}
  

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
   
    {{-- {{ HTML::style('css/sb-admin.css') }} 
	{{ HTML::style('css/sb-parsley.css') }} --}}
    

    <!-- datatables css -->
{{-- 
    {{ HTML::style('media/css/jquery.dataTables.min.css') }}

    {{ HTML::style('datepicker/css/bootstrap-datepicker.css') }}

    {{ HTML::style("https://fonts.googleapis.com/css?family=Roboto:300,400") }} --}}

    <style type="text/css">

   .right-inner-addon {
    position: relative;
   }
   .right-inner-addon input {
    padding-right: 30px;    
   }
   .right-inner-addon i {
    position: absolute;
    right: 0px;
    padding: 10px 12px;
    pointer-events: none;
   }

   .ui-datepicker {
    padding: 0.2em 0.2em 0;
    width: 550px;
   }
   </style>


    <!-- jquery scripts with datatable scripts -->

   <script src="{{asset('media/js/jquery.js')}}"></script>
   <script src="{{asset('media/js/jquery.dataTables.js')}}"></script>
   <script src="{{asset('js/parsley.js')}}"></script>
   <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}"></script>
   <script src="{{asset('js/velocity/velocity.js')}}"></script>
   <script src="{{asset('js/velocity/velocity.ui.js')}}"></script>
   <script src="{{asset('js/price_format.js')}}"></script>
    
    {{-- {{ HTML::script('media/js/jquery.js') }}

    {{ HTML::script('media/js/jquery.dataTables.js') }}
	{{ HTML::script('js/parsley.js') }}

    {{ HTML::script('datepicker/js/bootstrap-datepicker.js') }}

    {{ HTML::script('js/velocity/velocity.js') }}

    {{ HTML::script('js/velocity/velocity.ui.js') }}

    {{HTML::script('js/price_format.js') }} --}}

    
  <script type="text/javascript">

    $(document).ready(function() {
        /* Animations */
        //$("#side-menu li").velocity("transition.slideDownIn", {stagger: 100}, 2000);
        //$('.panel').velocity("transition.slideDownBigIn", 2000);
        //$('.row').velocity("transition.slideLeftIn", 2000)
        //$("table").velocity("transition.slideDownIn", 2000);
        //$("form").velocity("transition.slideDownIn", 2000);

        $('#users').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'}) ;
        $('#user2').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'}) ;

        $('table.users').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'}) ;
        $('#mobile').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'});
        $('#rejected').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'});
        $('#app').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'});
        $('#disbursed').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'});
        $('#amended').DataTable({"sDom": '<"H"lfrp>t<"F"ip>'});

    } );

</script>

<script type="text/javascript">

$(function(){
$('.datepicker').datepicker({
    format: 'dd-M-yyyy',
    startDate: '-60y',
    endDate: '+0d',
    autoclose: true
});
});

</script>

<script type="text/javascript">
$(function(){
$('.datepicker1').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-60y',
    endDate: '-18y',
    autoclose: true
});

$('.expiry').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '0y',
    autoclose: true
});

});
</script>

<script type="text/javascript">
$(function(){
$('.datepicker2').datepicker({
    format: "m-yyyy",
    startView: "months", 
    minViewMode: "months",
    startDate: '-2y',
    endDate: '+0y',
    autoclose: true
});
});
</script>

<script type="text/javascript">
$(function(){
$('.datepicker42').datepicker({
    format: " yyyy",
    startView: "years", 
    minViewMode: "years",
    startDate: '-2y',
    endDate: '+0y',
    autoclose: true
});
});
</script>

<script type="text/javascript">
$(function(){
$('.datepicker21').datepicker({
    format: "yyyy-mm-dd",
    assumeNearbyYear: true,   
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true
});

});
</script>

<script type="text/javascript">
$(function(){
$('.datepicker28').datepicker({
    format: "m-yyyy",
    startView: "months", 
    minViewMode: "months",
    autoclose: true
});
});
</script>

<script type="text/javascript">
$(function(){ 

$('.year').datepicker({
    format: " yyyy", // Notice the Extra space at the beginning
    viewMode: "years", 
    minViewMode: "years",
    endDate: '+0d',
    autoclose: true
});
});

</script>

<script language="javascript" type="text/javascript"> 
var timer = 0;
function set_interval() {
  // the interval 'timer' is set as soon as the page loads
  timer = setInterval("auto_logout()", 1800000);
  // the figure '10000' above indicates how many milliseconds the timer be set to.
  // Eg: to set it to 5 mins, calculate 5min = 5x60 = 300 sec = 300,000 millisec.
  // So set it to 300000
}

function reset_interval() {
  //resets the timer. The timer is reset on each of the below events:
  // 1. mousemove   2. mouseclick   3. key press 4. scrolling
  //first step: clear the existing timer

  if (timer != 0) {
    clearInterval(timer);
    timer = 0;
    // second step: implement the timer again
    timer = setInterval("auto_logout()", 1800000);
    // completed the reset of the timer
  }
}

function auto_logout() {
  // this function will redirect the user to the logout script
  window.location = "{{ URL::to('users/logout') }}";
}
</script>
<script type="text/javascript">
 $(document).ready(function () {
        $('#year').hide();
        $('#select_date').hide();
        $('#month').hide();
        $('#custom').hide();


        $('#period').change(function () {
            if ($(this).val() == "As at date" || $(this).val() == "day") {
                $('#year').hide();
                $('#select_date').show();
                $('#month').hide();
                $('#custom').hide();
            }else if ($(this).val() == "year") {
                $('#year').show();
                $('#select_date').hide();
                $('#month').hide();
                $('#custom').hide();
            } else if ($(this).val() == "month") {
              $('#year').hide();
              $('#select_date').hide();
              $('#month').show();
              $('#custom').hide();

            } else if ($(this).val() == "custom") {
              $('#year').hide();
              $('#select_date').hide();
              $('#month').hide();
              $('#custom').show();

            }
            else {
                $('#year').hide();
                $('#select_date').hide();
                $('#month').hide();
                $('#custom').hide();
            }
        });
    });


</script>
</head>

@extends('layouts.accounting')
@section('content')
    <h1>particulars</h1>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a class="btn btn-info btn-sm" href="{{ route('particulars.create')}}">New</a>
                </div>
                <div class="panel-body">
                    <table id="particulars" class="table table-condensed table-bordered table-responsive table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Credit Account</th>
                            <th>Debit Account</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($particulars as $particular)
                            <tr>
                                <td> {{ $i }}</td>
                                <td>{{ $particular['name'] }}</td>
                                <td>{{ $particular['credit_account'] }}</td>
                                <td>{{ $particular['debit_account'] }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a role="button" class="btn btn-info btn-sm dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            Action <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a class="btn btn-primary" href="{{url('particulars/'.$particular['id'].'/edit/')}}" role="button" style="width:100%; margin-bottom: 5px;">Update</a>
                                            </li>
                                            <li>
                                                <form action="{{url('particulars/'.$particular['id'])}}" method="post">
                                                    <input type="hidden" name="_method" value="delete">
                                                    <button type="submit" class="btn btn-danger" style="width: 100%;">Delete</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
   $(document).ready(function() {
     $('#particulars').DataTable({
         aaSorting: []
     });
 	} );
 </script>
@endsection

 @extends('layouts.accounting')
@section('content')
    <br/>

    <div class="row">
        <div class="col-lg-12">
            <h3>{{ $particular->name }}</h3>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif

            <form method="POST" action="{{{ URL::to('particulars/'. $particular->id)}}}"
                  accept-charset="UTF-8"data-parsley-validate>
                <input type="hidden" name="_method" value="put">
                <fieldset>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" minlength="2" data-parsley-trigger="change focusout"placeholder="Name" type="text" name="name" id="name"
                               value="{{{ $particular->name }}}" required>
                    </div>
                    <div class="form-group">
                        <label for="debit_account">Debit Account</label>
                        <select class="form-control selectable" name="debit_account" id="debit_account" required>

                            <option></option>
                            @foreach($accounts as $account)
                                <option value="{{ $account->id }}"
                                        @if($particular->debitAccount != null && $account->id == $particular->debitaccount_id)
                                        selected
                                        @endif
                                >{{ $account->name."(".$account->code.")" }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="credit_account">Credit Account</label>
                        <select class="form-control selectable" name="credit_account" id="credit_account" required>
                            <option></option>
                            @foreach($accounts as $account)
                                <option value="{{ $account->id }}"
                                  @if($particular->creditAccount != null && $account->id == $particular->creditAccount->id)
                                  selected
                                  @endif
                                >{{ $account->name."(".$account->code.")" }}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- <div class="form-group">
                        <label for="is_erp" class="form-group" >ERP particular</label>
                        <input type="checkbox" name="is_erp" value="1" @if($particular->is_erp == 1) checked @endif>
                    </div> -->
                    <input type="hidden" name="user" value="{{ Confide::user()->username }}">

                    <div class="form-actions form-group">

                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>
@stop

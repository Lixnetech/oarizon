<?php

use Illuminate\Database\Eloquent\Model;

class Particular extends Model {
	protected $fillable = [];
	protected $table = 'particulars';

	public function debitAccount(){
		return $this->belongsTo("Account", "debitaccount_id", "id");
	  }
	  
	  public function creditAccount(){
		return $this->belongsTo("Account", "creditaccount_id", "id");
	  }
}
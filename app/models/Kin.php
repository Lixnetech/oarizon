<?php

use Illuminate\Database\Eloquent\Model;

class Kin extends Model {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];


	public function member(){

		return $this->belongsTo('Kin');
	}

}
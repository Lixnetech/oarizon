<?php

use Illuminate\Database\Eloquent\Model;

class AccountTransaction extends Model{

	protected $table = 'account_transactions';

	// Validation Rules
	public static $rules = [
		// Rules come here....
	];

	// Link with Account model
	public function account(){
		return $this->belongsTo('Account');
	}

	// Link with PettycashItem model
	public function pettycashItem(){
		return $this->hasMany('PettycashItem');
	}

	// Link bank account StmtTransaction Model
	/*public function stmtTransaction(){
		return $this->belongsTo('StmtTransaction');
	}*/

	// Create a new Transaction
	public function createTransaction($data){
		$acTr = new AccountTransaction;

		if(array_key_exists("pay_method",$data)){
			$paymethod=$data['pay_method']; 
		}else{$paymethod="";}
		if(array_key_exists("pay_details",$data)){
			$paydetails=$data['pay_details'];
		}else{$paydetails="";}

		$acTr->transaction_date = $data['date'];
		$acTr->description = $data['description'];
		$acTr->account_debited = $data['debit_account']; 
		$acTr->account_credited = $data['credit_account'];
		$acTr->transaction_amount = $data['amount']; 
		$pettyCashItem->transaction_form = $paymethod;
		$pettyCashItem->form_details = $paydetails; 
		$acTr->save();

		return $acTr->id;
	}
}
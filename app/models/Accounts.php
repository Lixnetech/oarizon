<?php

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model {

	 protected $table = 'account';

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'category' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = [];

}
<?php

use Illuminate\Database\Eloquent\Model;

class StmtTransaction extends Model {

	protected $table = 'stmt_transactions';

	/**
	 * Link with BankStatement
	 */
	public function bankStatement(){
		return $this->belongsTo('BankStatement');
	}

	// Link with AccountTransaction Model
	/*public function accountTransaction(){
		return $this->hasOne('AccountTransaction');
	}*/
}
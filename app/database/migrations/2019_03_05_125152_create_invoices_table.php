<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table){
			$table->increments('id');
			$table->string('invoice_no')->nullable();
			$table->string('client_id')->nullable();
			$table->string('order_id')->nullable();
			$table->float('invoice_amount')->nullable();
			$table->float('amount_paid')->nullable();
			$table->float('balance')->nullable();
			$table->boolean('status')->false();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

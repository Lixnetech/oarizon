<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::create('delivery_notes', function(Blueprint $table)
		// {
		// 	$table->increments('id');
		// 	$table->string('receiptNo');
		// 	$table->integer('client_id')->unsigned();
		// 	$table->integer('user_id')->unsigned();
		// 	$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict');
		// 	$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
		// 	$table->date('date');
		// 	$table->timestamps();
		// });

	// 	Schema::create('delivery_items', function(Blueprint $table)
	// 	{
	// 		$table->increments('id');
	// 		$table->integer('delivery_note_id')->unsigned();
	// 		$table->integer('item_id')->unsigned();
	// 		$table->foreign('delivery_note_id')->references('id')->on('delivery_notes')->onDelete('cascade');
	// 		$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
	// 		$table->integer('quantity');
	// 		$table->timestamps();


	// 	});
	 }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_notes');
		Schema::drop('delivery_items');
	}

}

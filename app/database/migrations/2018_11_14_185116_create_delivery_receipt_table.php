<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryReceiptTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	/*	Schema::create('delivery_receipt', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('receiptNo');
			$table->unsignedInteger('client_id');
			$table->date('date')->default(DB::raw('DATE'));
		});

		Schema::create('delivery_items', function(Blueprint $table){
			$table->unsignedInteger('delivery_receipt_id')->index();
			$table->unsignedInteger('item_id')->index();
			$table->integer('quantity');

			$table->foreign('delivery_receipt_id')->references('delivery_receipt')->on('id')->onDelete('cascade');
			$table->foreign('item_id')->references('items')->on('id')->onDelete('cascade');
		});*/
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_receipt');
		Schema::drop('delivery_items');
	}

}

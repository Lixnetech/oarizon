<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParticularsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('particulars', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('creditaccount_id')->nullable();
			$table->integer('debitaccount_id')->nullable();
			$table->boolean('void')->default(false);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('particulars');
	}

}
